﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ezilineezitaskernew.Startup))]
namespace ezilineezitaskernew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using ezilineezitaskernew.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Globalization;
using ICSharpCode.SharpZipLib.Zip;
using System.Net.Mime;

namespace ezilineezitaskernew.Controllers
{
    public class HomeController : Controller
    {

        EzitaskerEntities dbcontext = new EzitaskerEntities();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        //Get Main Page(Home Page)

        [HttpGet]
        public ActionResult Index()
        {
            GetOffices();
            SetLogo();
            int companyid = Convert.ToInt32(Session["company-id"]);
            var screenshotlist = dbcontext.sp_getrecentscreenshots(null, null, companyid).ToList();

            int s_startId = screenshotlist[0].Id;
            int s_lastId = s_startId + 6;
            if (screenshotlist != null)
            {
                var screenshots = dbcontext.sp_getrecentscreenshots(s_startId, s_lastId, companyid).ToList();
                Session["recentscreenshots"] = screenshots;
            }

            //ViewBag.totaltime = obj.totaltime[0].Value;
            //ViewBag.totalproject = obj.totalproject[0].Value;
            // ViewBag.totaltask = obj.totaltask[0].Value;
            //  var list = dbcontext.sp_gettotaldetails().ToList();

            // ViewBag.totaltime = list[0];
            //ViewBag.totalprojects = list[0];

            return View();
        }
        public ActionResult Errorpage()
        {

            return View();
        }

        public ActionResult SetLogo()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["company-id"])))
            {
                int companyid = (Convert.ToInt32(Session["company-id"]));

                int companyId = Convert.ToInt32(companyid);
                sp_getcompanydetail_Result1 result = Getcompanydetail(companyId);
                ViewBag.logo = result.logo;
                ViewBag.Name = result.Name;
                return View("Index", "Home");
            }
            else
            {
                return RedirectToAction("logout", "Account");
            }
        }
        public void GetUsersbyentity()
        {
            var allusers = dbcontext.USERS.ToList();
            Session["userslist"] = allusers;
        }

        // [HttpGet]
        //View Page for adding users
        //public ActionResult AddUser(int? id)
        //{
        //    if (id != null)
        //    {
        //        USER obj = dbcontext.USERS.Where(x => x.Id == id).SingleOrDefault();
        //        return View(obj);
        //    }
        //    else
        //    {
        //        if (TempData["msg"] != null)
        //        {

        //            ViewBag.msg = TempData["msg"];
        //            GetRolls();
        //            GetCompanies(null);
        //            GetOffices();
        //            return View();
        //        }
        //        else
        //        {
        //            GetRolls();
        //            GetCompanies(null);
        //            GetOffices();
        //            return View();
        //        }
        //    }
        //}
        //[HttpPost]
        //[ActionName("AddUser")]
        ////Post Method to perform Insertion and Updation
        //public async Task<ActionResult> AddUser_post(Usermetadata model, string rollist)
        //{
        //    if (Session["adminname"].ToString() == "SuperAdmin")
        //    {
        //        if (model.Id != 0)
        //        {
        //            //dbcontext.Entry(user).State = EntityState.Modified;
        //            //dbcontext.SaveChanges();
        //            //TempData["msg"] = "Successfully Updated";
        //            return RedirectToAction("Adduser", "Home");
        //        }
        //        else
        //        {
        //            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //            var result = await UserManager.CreateAsync(user, model.Password);
        //            USER obj = new USER();
        //            // GetRolls();
        //            GetCompanies(null);
        //            //GetOffices();
        //            //Getbiomatricfeatures();
        //            obj.Address = model.Address;
        //            obj.Mobile = model.Mobile;
        //            obj.Email = model.Email;
        //            obj.Name = model.Name;
        //            obj.Username = model.Username;
        //            obj.Cnic = model.Cnic;
        //            obj.Password = model.Password;
        //            obj.ConfirmPassword = model.Password;
        //            //obj.RollName = "Admin";

        //            obj.RoleID = model.RoleID;
        //            obj.RoleName = GetNameByID(obj.RoleID);
        //            // obj.RollID = GetIDByName(obj.RollName);
        //            obj.AdminID = Session["adminid"].ToString();
        //            obj.CompanyID = model.CompanyID;
        //            obj.OfficeID = null;
        //            obj.LoginRole = "";
        //            obj.emp_finger = "";
        //            obj.Gender = model.Gender;
        //            //var Image = Request.Files;
        //            //string imgname = string.Empty;
        //            string ImageName = string.Empty;
        //            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
        //            {
        //                var logo = System.Web.HttpContext.Current.Request.Files["file"];
        //                if (logo.ContentLength > 0)
        //                {
        //                    var profileName = Path.GetFileName(logo.FileName);
        //                    var ext = Path.GetExtension(logo.FileName);
        //                    ImageName = profileName;
        //                    var comPath = Server.MapPath("../AdminImages/") + ImageName;
        //                    // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
        //                    logo.SaveAs(comPath);
        //                    model.Image = comPath;
        //                    var l = model.Image.Length;
        //                }
        //            }
        //            //string Name = Path.GetFileName(imagename);
        //            // string path = Server.MapPath("~/AdminAssets/images");
        //            // string fullpath = Path.Combine(path, Name);
        //            //model.Image.SaveAs(fullpath);
        //            //obj.Image = model.Image.FileName;
        //            //obj.Image = ImageName;
        //            obj.Description = model.Description;
        //            // obj.Status = model.Status;
        //            if (result.Succeeded)
        //            {
        //                ApplicationDbContext context = new ApplicationDbContext();
        //                UserManager.AddToRole(user.Id, obj.RoleName);
        //                dbcontext.USERS.Add(obj);
        //                try
        //                {
        //                    dbcontext.SaveChanges();
        //                }
        //                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        //                {
        //                    var err = dbEx;

        //                }
        //                TempData["msg"] = "Successfully Inserted";
        //                return RedirectToAction("ViewUser", "Home");
        //            }
        //            return RedirectToAction("ViewUser", "Home");
        //        }
        //    }
        //    else
        //    {
        //        if (model.Id != 0)
        //        {
        //            //dbcontext.Entry(user).State = EntityState.Modified;
        //            //dbcontext.SaveChanges();
        //            //TempData["msg"] = "Successfully Updated";
        //            return RedirectToAction("Adduser", "Home");
        //        }
        //        else
        //        {
        //            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //            var result = await UserManager.CreateAsync(user, model.Password);
        //            USER obj = new USER();
        //            GetRolls();
        //            GetCompanies(null);
        //            GetOffices();
        //            Getbiomatricfeatures();
        //            obj.Address = model.Address;
        //            obj.Mobile = model.Mobile;
        //            obj.Email = model.Email;
        //            obj.Name = model.Name;
        //            obj.Username = model.Username;
        //            obj.Cnic = model.Cnic;
        //            obj.Password = model.Password;
        //            obj.ConfirmPassword = model.Password;
        //            obj.RoleName = "Employee";
        //            obj.RoleID = GetIDByName(obj.RoleID);
        //            obj.AdminID = Session["adminid"].ToString();
        //            obj.CompanyID = model.CompanyID;
        //            obj.OfficeID = model.OfficeID;
        //            obj.LoginRole = model.LoginRole;
        //            obj.emp_finger = "";
        //            obj.Gender = model.Gender;
        //            var Image = Request.Files;
        //            string imgname = string.Empty;
        //            string ImageName = string.Empty;
        //            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
        //            {
        //                var logo = System.Web.HttpContext.Current.Request.Files["file"];
        //                if (logo.ContentLength > 0)
        //                {
        //                    var profileName = Path.GetFileName(logo.FileName);
        //                    var ext = Path.GetExtension(logo.FileName);
        //                    ImageName = profileName;
        //                    // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
        //                    var comPath = Server.MapPath("../AdminImages/") + ImageName;
        //                    logo.SaveAs(comPath);
        //                    model.Image = comPath;
        //                    var l = model.Image.Length;
        //                }
        //            }
        //            //string Name = Path.GetFileName(imagename);
        //            // string path = Server.MapPath("~/AdminAssets/images");
        //            // string fullpath = Path.Combine(path, Name);
        //            //model.Image.SaveAs(fullpath);
        //            //obj.Image = model.Image.FileName;
        //            //obj.Image = ImageName;
        //            obj.Description = model.Description;
        //            // obj.Status = model.Status;
        //            if (result.Succeeded)
        //            {
        //                ApplicationDbContext context = new ApplicationDbContext();
        //                UserManager.AddToRole(user.Id, obj.RoleName);
        //                dbcontext.USERS.Add(obj);
        //                try
        //                {
        //                    dbcontext.SaveChanges();
        //                }
        //                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        //                {
        //                    var err = dbEx;
        //                }
        //                TempData["msg"] = "Successfully Inserted";
        //                return RedirectToAction("ViewUser", "Home");
        //            }
        //            return RedirectToAction("ViewUser", "Home");
        //        }
        //    }
        //}

        //protected void GetOfficesbyentity()
        //{
        //    var officelist = dbcontext.OfficeLocs.ToList();
        //    Session["alloffices"] = officelist;

        //}

        public static string static_company_Id;
        public ActionResult GetOffices()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["company-id"])))
            {
                int companyId = (Convert.ToInt32(Session["company-id"]));
                string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                List<OfficeLoc> list = new List<OfficeLoc>();
                using (SqlConnection con = new SqlConnection(cs))
                {
                    string query = "select * from tasker_usr.OfficeLoc where companyId =" + companyId;
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        OfficeLoc office = new OfficeLoc();
                        office.Id = Convert.ToInt32(dr["Id"]);
                        office.Name = dr["Name"].ToString();
                        list.Add(office);

                    }
                    SelectList listt = new SelectList(list, "Id", "Name");
                    ViewBag.Officelist = listt;
                }
                return View();
            }
            else
            {

                return RedirectToAction("logout", "Account");
            }
        }
        public void Getbiomatricfeatures()
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<tbl_setpermission> list = new List<tbl_setpermission>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "select * from tbl_setpermission where (Feature='Biomatric' or Feature='screenrecorder' or Feature = 'Bioandscreenrecorder')  and SetPermission=1 and Id=" + Session["additionalId"];
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    tbl_setpermission role = new tbl_setpermission();
                    //if (dr["Name"].ToString() != "SuperAdmin")
                    //{
                    role.PermissionID = Convert.ToInt32(dr["permissionid"]);
                    role.Feature = dr["feature"].ToString();
                    list.Add(role);
                    //   }
                }
                SelectList listt = new SelectList(list, "permissionid", "feature");
                ViewBag.features = listt;
            }
        }
        public string GetIDByName(string name)
        {
            List<AspNetRole> list = dbcontext.AspNetRoles.Where(x => x.Name == name).ToList();
            if (list.Count > 0)
            {

                return list[0].Id;
            }
            else
            {
                return "";
            }
        }
        public string GetNameByID(string id)
        {
            List<AspNetRole> list = dbcontext.AspNetRoles.Where(x => x.Id == id).ToList();
            if (list.Count > 0)
            {

                return list[0].Name;
            }
            else
            {
                return "";
            }
        }
        public void GetRolls()
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<AspNetRole> list = new List<AspNetRole>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "select * from aspNetroles";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    AspNetRole role = new AspNetRole();
                    if (dr["Name"].ToString() != "SuperAdmin")
                    {
                        role.Id = dr["id"].ToString();
                        role.Name = dr["Name"].ToString();
                        list.Add(role);
                    }
                }
                SelectList listt = new SelectList(list, "id", "Name");
                ViewBag.rolllist = listt;
            }
        }
        public void GetAdminsByID()
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<USER> list = new List<USER>();

            using (SqlConnection con = new SqlConnection(cs))
            {
                var adminid = "";
                var alladmins = dbcontext.AspNetRoles.Where(x => x.Name == "Admin").ToList();
                if (alladmins.Count > 0)
                {
                    adminid = alladmins[0].Id;
                    string query = "select * from tasker_usr.USERS where RoleId='" + adminid + "'";
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        USER role = new USER();
                        role.Id = Convert.ToInt32(dr["Id"]);
                        role.Name = dr["Name"].ToString();
                        list.Add(role);
                    }
                    SelectList listt = new SelectList(list, "Id", "Name");
                    ViewBag.userslist = listt;
                }
                else
                {
                    SelectList listt = new SelectList(list, -1, "There is no record found");
                    ViewBag.userslist = listt;

                }
            }
        }
        public void GetUsersByAdminId(string id)
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<USER> list = new List<USER>();

            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "select * from tasker_usr.USERS Where AdminID='" + id + "'";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    USER role = new Models.USER();
                    role.Id = Convert.ToInt32(dr["Id"]);
                    role.Name = dr["Name"].ToString();
                    list.Add(role);
                }
                ViewBag.userslist = new SelectList(list, "Id", "Name");
            }
        }
        public void GetCompanies(int? id)
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<tbl_company> list = new List<tbl_company>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "";
                if (id != null)
                {
                    query = "select * from tbl_company where companyid=" + id;
                }
                else
                {
                    query = "select * from tbl_company";
                }
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    tbl_company company = new tbl_company();
                    company.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    company.Name = dr["Name"].ToString();
                    list.Add(company);
                }
                SelectList listt = new SelectList(list, "CompanyID", "Name");
                ViewBag.companylist = listt;
            }
        }
        public void GetCompaniesByID(int id)
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<tbl_company> list = new List<tbl_company>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "select c.CompanyID,c.Name from tasker_usr.USERS i inner join tbl_company c on c.CompanyID = i.CompanyID where Id=" + id;
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    tbl_company company = new tbl_company();

                    company.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    company.Name = dr["Name"].ToString();
                    list.Add(company);
                }
                SelectList listt = new SelectList(list, "CompanyID", "Name");
                ViewBag.companylistt = listt;
            }
        }
        [HttpGet]
        //View Page of all users

        public void GetUsers()
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<USER> list = new List<USER>();

            using (SqlConnection con = new SqlConnection(cs))
            {
                var adminid = "";
                var alladmins = dbcontext.AspNetRoles.Where(x => x.Name == "Admin").ToList();
                if (alladmins.Count > 0)
                {
                    adminid = alladmins[0].Id;
                    string query = "select * from tasker_usr.USERS";
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        USER role = new USER();
                        role.Id = Convert.ToInt32(dr["Id"]);
                        role.Name = dr["Name"].ToString();
                        list.Add(role);
                    }
                    SelectList listt = new SelectList(list, "Id", "Name");
                    ViewBag.userslist = listt;
                }
                else
                {
                    SelectList listt = new SelectList(list, -1, "There is no record found");
                    ViewBag.userslist = listt;

                }
            }
        }

        //[HttpGet]
        //public ActionResult GetUsers()
        //{
        //    var allusers = dbcontext.USERS.ToList();
        //    Session["allusers"] = allusers;
        //    return View();
        //}

        [HttpGet]
        public ActionResult ViewCompany()
        {
            GetOffices();
            SetLogo();
            var list = dbcontext.tbl_company.ToList();
            if (list.Count > 0)
            {
                return View("ViewCompany", list);
            }
            else
            {
                return View("ViewCompany", null);
            }

        }

        [HttpPost]
        public ActionResult DeleteCompany(int? id)
        {
            if (id != 0)
            {
                tbl_company obj = dbcontext.tbl_company.Where(x => x.CompanyID == id).FirstOrDefault();
                dbcontext.tbl_company.Remove(obj);
                dbcontext.SaveChanges();
                return RedirectToAction("ViewCompany");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult DeleteProjects(int id)
        {
            if (id != 0)
            {
                Project_Code obj = dbcontext.Project_Code.Where(x => x.Id == id).FirstOrDefault();
                dbcontext.Project_Code.Remove(obj);
                dbcontext.SaveChanges();
                return RedirectToAction("ViewProject");
            }
            else
            {
                return View();
            }
        }
        [HttpPost]

        public List<OfficeLoc> LoadAllOffices()
        {
            int companyId = Convert.ToInt32(Session["company-id"]);
            var list = dbcontext.OfficeLocs.Where(x => x.CompanyID == companyId).ToList();
            return list;
        }
        public ActionResult DeleteOffices(int delid)
        {
            if (delid > 0)
            {
                OfficeLoc obj = dbcontext.OfficeLocs.Where(x => x.Id == delid).SingleOrDefault();
                if (obj != null)
                {
                    dbcontext.OfficeLocs.Remove(obj);
                    dbcontext.SaveChanges();
                    var office = LoadAllOffices();
                    delid = 0;
                    return Json("Ok", JsonRequestBehavior.AllowGet);
                }
                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Not Ok");
            }
        }
        [HttpPost]
        public ActionResult DeleteRole(string id)
        {
            if (id != "")
            {

                AspNetRole obj = dbcontext.AspNetRoles.Where(x => x.Id == id).FirstOrDefault();
                dbcontext.AspNetRoles.Remove(obj);
                dbcontext.SaveChanges();
                return RedirectToAction("ViewRole");
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        //View Page for adding Company
        public ActionResult CreateCompany()
        {
            if (TempData != null)
            {
                ViewBag.msg = TempData["msg"];
                var list = dbcontext.tbl_company.ToList();
                if (list.Count > 0)
                {
                    return View(list);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                var list = dbcontext.tbl_company.ToList();
                if (list.Count > 0)
                {
                    return View(list);
                }
                else
                {
                    return View();
                }

            }
        }
        [HttpPost]
        [ActionName("CreateCompany")]
        //Post Method to perform insertion and updation
        public ActionResult CreateCompany_post(tbl_company company)
        {
            if (ModelState.IsValid)
            {
                tbl_company obj = new tbl_company();
                var comPath = "";
                obj.Name = company.Name;
                var Image = Request.Files;
                string imgname = string.Empty;
                string ImageName = string.Empty;
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var logo = System.Web.HttpContext.Current.Request.Files["file"];
                    if (logo.ContentLength > 0)
                    {
                        var profileName = Path.GetFileName(logo.FileName);
                        var ext = Path.GetExtension(logo.FileName);
                        ImageName = profileName;
                        comPath = Server.MapPath("../AdminImages/") + ImageName;
                        //comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
                        logo.SaveAs(comPath);
                        company.Logo = comPath;
                        var l = company.Logo.Length;
                    }
                }
                obj.Logo = ImageName;
                obj.Address = company.Address;
                obj.Description = company.Description;
                obj.AdminID = Session["adminid"].ToString();
                dbcontext.tbl_company.Add(obj);
                dbcontext.SaveChanges();
                TempData["msg"] = "Company Added Successfully";
                // return Json(comPath);
                return RedirectToAction("Viewcompany", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult RolesPermission()
        {

            //int additionalid = Convert.ToInt32(model.additionalid);
            if (TempData["msg"] != null && TempData["msg"].ToString() != "")
            {
                ViewBag.msg = TempData["msg"];
                GetAdminsByID();
                // GetCompaniesbyid(Convert.ToInt32(model.additionalid));
                // GetCompanies();
                return View();
            }
            else
            {
                // GetCompaniesbyid(Convert.ToInt32(model.additionalid));
                //GetCompanies();
                GetAdminsByID();
                //tbl_setpermission obj = new tbl_setpermission();
                //if (model.additionalid == additionalid)
                //{
                //    List<tbl_setpermission> list = dbcontext.tbl_setpermission.Where(x => x.additionalid == model.additionalid).ToList();
                //    if (list.Count > 0)
                //    {
                //        //if (list[0].Feature == "AddUser" && list[0].SetPermission == true)
                //        //{
                //           // Session["AddUser"] = "checked";
                //          //  ViewData["AddUser"] = "checked";
                //          //  ViewBag.AddUser = "checked";
                //       // }
                //        //if (list[1].Feature == "ViewUser" && list[1].SetPermission == true)
                //        //{
                //        //    ViewData["ViewUser"] = "checked";
                //        //    ViewBag.ViewUser = "checked";
                //        //}
                //        return View();
                //    }
                //    else
                //    {
                //        return View();
                //    }
                //}
                //else
                //{
                // tbl_setpermission list=dbcontext.tbl_setpermission.SingleOrDefault();
                var list = dbcontext.tbl_setpermission.ToList();
                if (list != null)
                {
                    //if (list[0].Feature == "AddUser" && list[0].SetPermission == true)
                    //{
                    //    Session["AddUser"] = "checked";
                    //    ViewData["AddUser"] = "checked";
                    //    ViewBag.AddUser = "checked";
                    //}
                    //if (list[1].Feature == "ViewUser" && list[1].SetPermission == true)
                    //{
                    //    ViewBag.ViewUser = "checked";
                    //}
                    string output = JsonConvert.SerializeObject(list);
                    return View(list);
                }
                else
                {
                    //GetCompaniesbyid(Convert.ToInt32(model.additionalid));
                    ////GetCompanies();
                    //GetAdminsById();

                    //GetRolls();
                    //GetCompanies();
                    return View();
                }
            }
            //}
            //List<tbl_company> list = dbcontext.tbl_company.ToList();
            ////list.Insert(0, new tbl_company( , Name))
            //ViewBag.List = new SelectList(list, "CompanyID", "Name");
        }
        [HttpPost]
        [ActionName("RolesPermission")]
        public ActionResult RolesPermission_post(tbl_setpermission model)
        {
            if (ModelState.IsValid)
            {
                List<tbl_setpermission> list = dbcontext.tbl_setpermission.Where(x => x.Id == model.Id && x.Feature == model.Feature).ToList();
                if (list.Count > 0)
                {
                    var permission = model.SetPermission;
                    dbcontext.sp_updatepermission(list[0].PermissionID, Convert.ToBoolean(permission));
                    TempData["msg"] = "Permission set successfully";
                    return RedirectToAction("RolesPermission", "Home");
                }
                else
                {
                    tbl_setpermission obj = new tbl_setpermission();
                    obj.Feature = model.Feature;
                    obj.SetPermission = model.SetPermission;

                    obj.Id = model.Id;
                    dbcontext.tbl_setpermission.Add(obj);
                    dbcontext.SaveChanges();
                    TempData["msg"] = "Permission set successfully";
                    return RedirectToAction("RolesPermission", "Home");
                }
            }
            else
            {
                return RedirectToAction("RolesPermission", "Home");
            }
        }
        //[HttpGet]
        //public void checkPermission()
        //{
        //    if (Session["adminid"].ToString() != "")
        //    {

        //        if (Session["adminname"].ToString() == "Admin")
        //        {
        //            string s = Session["additionalId"].ToString();
        //            var list = dbcontext.sp_checkpermission(s).ToList();
        //            if (list.Count > 0)
        //            {
        //                Session["admin"] = "true";
        //                Session["featureKey"] = list;
        //            }
        //        }
        //    }
        //}
        [HttpGet]
        public ActionResult AddRole()
        {
            if (TempData["msg"].ToString() != "" && TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
                return View();
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        [ActionName("AddRole")]
        public ActionResult AddRole_post(AspNetRole model)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var rolemanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (!rolemanager.RoleExists(model.Name))
            {
                var role = new IdentityRole(model.Name);
                rolemanager.Create(role);
                TempData["msg"] = "Role Added Successfully";
                return RedirectToAction("ViewRole", "Home");
                //tbluser.RollID = role.Id;
                //tbluser.Roll = RollName;
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult AddProject()
        {
            GetUsersByAdminId(Session["adminid"].ToString());
            return View();
        }
        [HttpPost]
        [ActionName("AddProject")]
        public ActionResult AddProject_post(Project_Code model, int? updateid)
        {
            if (model.Id != 0)
            {
                var list = dbcontext.Project_Code.Where(x => x.Id == model.Id).SingleOrDefault();
                var json = new JavaScriptSerializer().Serialize(list);
                Session["updateval"] = model.Id;
                return Json(list);
            }
            //if (Session["updateval"] != null)
            //{
            //    int id = Convert.ToInt32(Session["updateval"]);
            //    dbcontext.sp_updateproject(model.Project_Name, model.Username, model.Date, model.Status, model.Notes, model.Show, id);
            //    // dbcontext.Entry(model).State = EntityState.Modified;
            //    //dbcontext.SaveChanges();
            //    TempData["msg"] = "Successfully Updated";
            //    return Json("");
            //}
            else
            {
                Project_Code obj = new Project_Code();
                obj.Project_Name = model.Project_Name;
                obj.Username = model.Username;
                obj.Date = model.Date;
                obj.Status = model.Status;
                obj.Notes = model.Notes;
                obj.Show = "Check";
                obj.Project_Hours = "";
                model.CompanyID = Convert.ToInt32(Session["company-id"]);
                dbcontext.Project_Code.Add(obj);
                dbcontext.SaveChanges();
                TempData["msg"] = "New User Created Successfully";
                return RedirectToAction("ViewProject", "Home");
            }
        }
        public ActionResult ViewProject()
        {
            GetUsersByAdminId(Session["adminid"].ToString());
            int companyid = Convert.ToInt32(Session["company-id"]);
            var list = dbcontext.Project_Code.Where(x => x.CompanyID == companyid).ToList();
            return PartialView("_ViewProject", list);
        }
        [HttpGet]
        public ActionResult GetDetailByName(string name)
        {
            var list = dbcontext.TaskTimes.Where(x => x.Username == name).ToList();
            if (list.Count > 0)
            {
                return PartialView("_GetDetailByName", list);
            }
            else
            {
                return PartialView("_GetDetailByName");
            }
        }

        [HttpGet]
        public ActionResult UserProfile(int? id)
        {
            var list = dbcontext.USERS.Where(x => x.Id == id).SingleOrDefault();
            return View(list);
        }
        public ActionResult ViewRole()
        {
            var list = dbcontext.AspNetRoles.ToList();
            return View(list);
        }
        [HttpGet]
        public ActionResult companydashboard(int id)
        {
            Session["company-id"] = id;
            //  int userid = Convert.ToInt32("");
            var userlisttotaltime = dbcontext.sp_getusertotaltime(id).ToList();
            ViewBag.usertotaltime = userlisttotaltime[0];
            var usertotalproject = dbcontext.sp_gettotalparoject(id).ToList();
            ViewBag.totalproject = usertotalproject[0];
            var useralltasks = dbcontext.sp_getalltasks(id).ToList();
            ViewBag.useralltasks = useralltasks[0];
            var getallscreenshots = dbcontext.sp_getallscreenshots(id).ToList();
            ViewBag.getallscreenshots = getallscreenshots[0];
            var biomatrichours = dbcontext.gettotaltimeadminbiometricnew(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.biomatrichours = biomatrichours[0];


            //This is current month calculation
            var getthismmonthtime = dbcontext.getthismonthtimeuser(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismonthtime = getthismmonthtime[0];

            var getthismmonthtask = dbcontext.getthismonthtask(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismmonthtask = getthismmonthtask[0];

            var getthismmonthscreenshot = dbcontext.sp_getthismonthscreenshots(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismmonthscreenshot = getthismmonthscreenshot[0];

            var getthismmonthmaxtime = dbcontext.AllMaxTime(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismmonthmaxtime = getthismmonthmaxtime[0];

            var getthismmonthbiohours = dbcontext.getthismonthtimeadminbiometric(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismmonthbiohours = getthismmonthbiohours[0];



            //This is Today calculation
            var gettodayhours = dbcontext.sp_gettodayhours(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.gettodayhours = gettodayhours[0];

            var gettodayproject = dbcontext.sp_gettodayproject(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.gettodayproject = gettodayproject[0];

            var gettodayTasks = dbcontext.sp_gettodayTasks(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.gettodayTasks = gettodayTasks[0];

            var gettodayscreenshots = dbcontext.sp_gettodayscreenshots(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.gettodayscreenshots = gettodayscreenshots[0];

            var gettodayBiohours = dbcontext.gettodaybiometrichours(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.gettodayBiohours = gettodayBiohours[0];


            var screenshotlist = dbcontext.sp_getrecentscreenshots(null, null , null).ToList();
            int s_startId = screenshotlist[0].Id;
            int s_lastId = s_startId + 6;
            if (screenshotlist != null)
            {
                var screenshots = dbcontext.sp_getrecentscreenshots(s_startId, s_lastId , id).ToList();
                Session["recentscreenshots"] = screenshots;
            }

            var projectlist = dbcontext.sp_getrecentprojects(null, null).ToList();
            List<sp_gettoptasks_Result> tasklist = null;
            int p_startid = projectlist[0].id;
            int p_lastid = p_startid + 2;
            List<sp_gettoptasks_Result> tasks = new List<sp_gettoptasks_Result>();
            if (projectlist != null)
            {
                var projects = dbcontext.sp_getrecentprojects(p_startid, p_lastid).ToList();
                foreach (var item in projects)
                {
                    tasklist = dbcontext.sp_gettoptasks(item.Project_Name).ToList();
                    tasks.Add(new sp_gettoptasks_Result { Task = tasklist[0].Task, status = tasklist[0].status });
                }
                Session["toptasks"] = tasks;
                Session["recentprojects"] = projects;
            }

            int userid = Convert.ToInt32(Session["additionalId"]);
            string adminid = Session["adminid"].ToString();
            if (Session["adminname"].ToString() == "SuperAdmin")
            {
                GetAdminsByID();
                var list = dbcontext.OfficeLocs.Where(x => x.CompanyID == id).ToList();
                return View("companydashboard", list);
            }
            //else
            //{
            //    // GetAdminsByID();
            //    var list = dbcontext.tasker_usr_OfficeLoc.Where(x => x.CompanyId == id && x.Additionalid == userid).ToList();
            //    if (list.Count > 0)
            //    {
            //        return View("companydashboard", list);
            //    }
            //    else
            //    {
            //        return View("companydashboard");
            //    }
            //}
            return View("companydashboard");
        }
        [HttpPost]
        [ActionName("companydashboard")]
        public ActionResult companydashboard_post(int? id)
        {
            Session["company-id"] = id;
            int userid = Convert.ToInt32(Session["additionalId"]);
            string adminid = Session["adminid"].ToString();
            if (Session["adminname"].ToString() == "SuperAdmin")
            {
                var list = dbcontext.OfficeLocs.ToList();
                return View("companydashboard", list);
            }
            else
            {
                var list = dbcontext.OfficeLocs.Where(x => x.CompanyID == id).ToList();
                if (list.Count > 0)
                {
                    return View("companydashboard", list);
                }
                else
                {
                    return View("companydashboard");
                }
            }
        }
        public ActionResult Complaint()
        {
            var list = dbcontext.Report_a_Problem.ToList();
            return View(list);
        }
        public ActionResult SessionReport()
        {
            var list = dbcontext.Todayworks.ToList();
            return View(list);
        }

        [HttpGet]
        public ActionResult Getmonthlyreport()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            //var getthismmonthtime = dbcontext.getthismonthtimeuser(Convert.ToInt32(Session["company-id"])).ToList();
            //ViewBag.getthismonthtime = getthismmonthtime[0];

            var getthismmonthtask = dbcontext.getthismonthtask(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismmonthtask = getthismmonthtask[0];

            var getthismmonthscreenshot = dbcontext.sp_getthismonthscreenshots(Convert.ToInt32(Session["company-id"])).ToList();
            ViewBag.getthismmonthscreenshot = getthismmonthscreenshot[0];

            //var getthismmonthmaxtime = dbcontext.AllMaxTime(Convert.ToInt32(Session["company-id"])).ToList();
            //ViewBag.getthismmonthmaxtime = getthismmonthmaxtime[0].Value.Hours;

            //var getthismmonthbiohours = dbcontext.getthismonthtimeadminbiometric(Convert.ToInt32(Session["company-id"])).ToList();
            //ViewBag.getthismmonthbiohours = getthismmonthbiohours[0];


            return Json(new { ThismonthTasks = ViewBag.getthismmonthtask, Thismonthscreenshots = ViewBag.getthismmonthscreenshot/*, Maxtime = ViewBag.getthismmonthmaxtime*/ }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult Gettodayreport()
        //{
        //    var gettodayproject = dbcontext.sp_gettodayproject(Convert.ToInt32(Session["company-id"])).ToList();
        //    ViewBag.gettodayproject = gettodayproject[0];

        //    var gettodayTasks = dbcontext.sp_gettodayTasks(Convert.ToInt32(Session["company-id"])).ToList();
        //    ViewBag.gettodayTasks = gettodayTasks[0];

        //    var gettodayscreenshots = dbcontext.sp_gettodayscreenshots(Convert.ToInt32(Session["company-id"])).ToList();
        //    ViewBag.gettodayscreenshots = gettodayscreenshots[0];
        //    return Json(new { TodayProject = ViewBag.gettodayproject, TodayTasks = ViewBag.gettodayTasks, TodayScreenshots = ViewBag.gettodayscreenshots }, JsonRequestBehavior.AllowGet);
        //}


        [HttpPost]
        [ActionName("Getthismonthtotalhours")]
        public ActionResult Getthismonthtotalhours_post()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var list = dbcontext.sp_getthismonthhour(companyid).ToList();
            return Json(new { data = list });
        }

        [HttpPost]
        public ActionResult Getthismonthbiohours()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var list = dbcontext.sp_getthismonthbiohours(companyid).ToList();
            return Json(new { data = list });
        }

        [HttpPost]
        public ActionResult Getthismonthttasks()
        {
            return View();
        }
        [HttpPost]
        //public ActionResult GetInfo()
        //{
        //    return View();
        //}
        public ActionResult GetInfo()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var list = dbcontext.sp_allhours(companyid).ToList();
            //   List<usertasktimemeta> obj = new List<usertasktimemeta>();


            foreach (var item in list)
            {
                //     TimeSpan totaltime = item.Total_Time;
                //     TimeSpan now = item.Total_Time;
                //     TimeSpan halfOfNow = new TimeSpan(now.Ticks);
                //string timeStamp = halfOfNow.ToString();
                //var segments = timeStamp.Split(':');
                //TimeSpan t = new TimeSpan(0, Convert.ToInt32(segments[0]),
                //                       Convert.ToInt32(segments[1]), Convert.ToInt32(segments[2]));
                //string time = string.Format("{0}:{1}:{2}",
                //                ((int)t.TotalHours), t.Minutes, t.Seconds);
                //decimal nowtime = Convert.ToDecimal(t.TotalHours);
                //    int standard = Convert.ToInt32(item.Standard_Time);
                //string prd = ((standard / nowtime) * 100).ToString();
                //float value = float.Parse(prd);
                //value = (float)System.Math.Round(value, 2);
                //    obj.Add(new usertasktimemeta
                //    {
                //        Id = item.Id,
                //        Username = item.Username,
                //        Start_Task = item.Start_Task,
                //        End_Task = item.End_Task,
                //        Task = item.Task,
                //        Total_Time = item.Total_Time,
                //        Screenshot = item.Screenshot,
                //        Standard_Time = item.Standard_Time,
                //        status = item.status,
                ////        productivity = 

                //    });
            }

            //foreach (var item in list)
            //{
            //    Nullable<TimeSpan> totaltime = item.Total_Time;
            //    Nullable<TimeSpan> now = item.Total_Time;
            //    TimeSpan halfOfNow = new TimeSpan(now.Value.Ticks);
            //    string timeStamp = halfOfNow.ToString();
            //    var segments = timeStamp.Split(':');
            //    TimeSpan t = new TimeSpan(0, Convert.ToInt32(segments[0]),
            //                           Convert.ToInt32(segments[1]), Convert.ToInt32(segments[2]));
            //    string time = string.Format("{0}:{1}:{2}",
            //                    ((int)t.TotalHours), t.Minutes, t.Seconds);
            //    decimal nowtime = Convert.ToDecimal(t.TotalHours);
            //    int standard = Convert.ToInt32(item.Standard_Time);
            //    string prd = ((standard / nowtime) * 100).ToString();
            //    float value = float.Parse(prd);
            //    value = (float)System.Math.Round(value, 2);
            //    ob.Add(new TaskTime()
            //    {
            //        Id = item.Id,
            //        Username = item.Username,
            //        starttask = item.Start_Task.ToString(),
            //        Endtask = item.End_Task.ToString(),
            //        Task = item.Task,
            //        totaltime = totaltime.ToString(),
            //        Screenshot = item.Screenshot,
            //        Standard_Time = item.Standard_Time,
            //        status = item.status,
            //        productivity = value + "%",
            //    });
            //}
            return Json(new { data = list });
        }

        [HttpPost]
        //public ActionResult Getuserhours(string username)
        //{
        //    int companyid = Convert.ToInt32(Session["company-id"]);
        //    var userlisttotaltime = dbcontext.sp_getsingleusertime(Convert.ToInt32(Session["company-id"]), username).ToList();
        //    ViewBag.usertotaltime = userlisttotaltime[0];

        //    var list = dbcontext.sp_getsingleusertasks(companyid, username).ToList();
        //    List<tblusertasktimemeta> obj = new List<tblusertasktimemeta>();
        //    foreach (var item in list)
        //    {
        //        TimeSpan totaltime = item.Total_Time;
        //        TimeSpan now = item.Total_Time;
        //        TimeSpan halfOfNow = new TimeSpan(now.Ticks);
        //        string timeStamp = halfOfNow.ToString();
        //        var segments = timeStamp.Split(':');
        //        TimeSpan t = new TimeSpan(0, Convert.ToInt32(segments[0]),
        //                               Convert.ToInt32(segments[1]), Convert.ToInt32(segments[2]));
        //        string time = string.Format("{0}:{1}:{2}",
        //                        ((int)t.TotalHours), t.Minutes, t.Seconds);
        //        decimal nowtime = Convert.ToDecimal(t.TotalHours);
        //        int standard = Convert.ToInt32(item.Standard_Time);
        //        string prd = ((standard / nowtime) * 100).ToString();
        //        float value = float.Parse(prd);
        //        value = (float)System.Math.Round(value, 2);
        //        obj.Add(new tblusertasktimemeta()
        //        {
        //            Id = item.Tasktimeid,
        //            Username = item.Username,
        //            Start_Task = item.Start_Task.ToString(),
        //            End_Task = item.End_Task.ToString(),
        //            Task = item.Task,
        //            Total_Time = totaltime,
        //            Screenshot = item.Screenshot,
        //            Standard_Time = item.Standard_Time,
        //            // status = item.Status,
        //            productivity = value + "%",
        //        });

        //    }
        //    string json = JsonConvert.SerializeObject(obj);
        //    //tbl_user_tasktime obj = new tbl_user_tasktime();
        //    //obj.Tasktimeid = list[0].Tasktimeid;
        //    //obj.Start_Task = list[0].Start_Task;
        //    //var v=list[0].Total_Time.Value.Minutes.ToString() +":"+ list[0].Total_Time.Value.Seconds.ToString();
        //    //var t = list[0].Total_Time;
        //    return Json(new { data = obj });
        //}
        //   [HttpPost]
        public ActionResult Gettotalproject()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var list = dbcontext.Project_Code.Where(x => x.CompanyID == companyid).ToList();
            return Json(new { data = list });
        }
        [HttpPost]
        public ActionResult GetBiomatricHours()
        {
            var list = dbcontext.sp_GetallBioHours().ToList();

            //foreach (var item in list)
            //{
            //    obj.Add(new tbluserbiomatric()
            //    {
            //        ID = item.ID,
            //        emp_id =
            //        item.emp_id,
            //        Username = item.Username,
            //        checkin_time = item.checkin_time,
            //        break_time = item.break_time,
            //        checkout_time = item.checkout_time,
            //        total_time = item.total_time,
            //        total_break_time = item.total_break_time,
            //        date = item.date,
            //        is_force_checkout = item.is_force_checkout
            //    });
            //}
            return Json(new { data = list });
        }
        public ActionResult Viewscreenshots(string id)
        {
            var list = dbcontext.Screenshots.Where(x => x.Task == id).ToList();
            return View("Viewscreenshots", list);

        }

        //public ActionResult Getchart()
        //{
        //    var list = dbcontext.tbl.ToList();
        //    var Values = list.Select(x => x.Values).Distinct();
        //    ViewBag.Values = Values;

        //    List<tbl_chart> r = new List<tbl_chart>();
        //    foreach (var item in list)
        //    {
        //        r.Add(new tbl_chart() { Values = item.Values });
        //    }
        //    return Json(list, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //[ActionName("Viewscreenshots")]
        //public ActionResult Viewscreenshots_post(string id)
        //{

        //    var list = dbcontext.tbl_userscreenshot.Where(x => x.Task == id).ToList();
        //    TempData["list"] = list;
        //    return RedirectToAction("Viewscreenshots");
        //}

        public void GetRoles()
        {
            string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            List<AspNetRole> list = new List<AspNetRole>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                string query = "select * from tbl_NetRoles";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    AspNetRole role = new AspNetRole();
                    if (dr["Name"].ToString() != "SuperAdmin")
                    {
                        role.Id = dr["id"].ToString();
                        role.Name = dr["Name"].ToString();
                        list.Add(role);
                    }
                }
                SelectList listt = new SelectList(list, "id", "Name");
                ViewBag.rolelist = listt;
            }
        }

        [HttpPost]
        public int GetidbyUsername(string username)
        {

            var list = dbcontext.USERS.Where(x => x.Username == username).SingleOrDefault();
            int UserId = list.Id;
            return UserId;
        }
        List<proclass> arrlist = new List<proclass>();
        List<proclass> userlist = new List<proclass>();
        List<proclass> todayuserlist = new List<proclass>();
        public ActionResult GetTodayReport(string usertext, string username = null)
        {
            Session["usertext"] = usertext;
            var gettotaltodayhours = "00";
            var gettotalIdlehours = "00";
            if (usertext != null)
            {
                int userId = GetidbyUsername(usertext);
                int companyid = Convert.ToInt32(Session["company-id"]);
                var getusertodayhours = dbcontext.sp_gettodayhoursbyusername(companyid, usertext).ToList();
                var getusertodayproject = dbcontext.sp_gettodayprojectbyusername(companyid, usertext).ToList();
                var getusertodaytasks = dbcontext.sp_gettodayTasksbyusername(companyid, usertext).ToList();
                var getusertodaybiohours = dbcontext.gettodaybiometrichoursbyusername(companyid, userId).ToList();
                if (getusertodayhours[0] != null)
                {
                    int todayuserhours = getusertodayhours[0].Value.Hours;
                    if (todayuserhours > 12)
                    {
                        todayuserhours = todayuserhours - 12;
                    }
                    if (todayuserhours == 0)
                    {
                        todayuserhours = Convert.ToInt32("00");
                    }

                    var minutes = getusertodayhours[0].Value.Minutes;
                    var seconds = getusertodayhours[0].Value.Seconds;
                    gettotaltodayhours = todayuserhours + ":" + minutes + ":" + seconds;

                }
                todayuserlist.Add(new proclass
                {
                    todayhoursofuser = gettotaltodayhours,
                    todayprojectofuser = getusertodayproject,
                    todaytasksofuser = getusertodaytasks,
                    Todaybiomatricofuser = getusertodaybiohours,
                });
                return Json(todayuserlist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(username))
                {
                    Session["username"] = username;
                    int userId = GetidbyUsername(username);
                    Session["userId"] = userId;
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var getusertotalhours = dbcontext.sp_gettodayhoursbyusername(companyid, username).ToList();
                    var getusertotalproject = dbcontext.sp_gettodayprojectbyusername(companyid, username).ToList();
                    var getusertotaltasks = dbcontext.sp_gettodayTasksbyusername(companyid, username).ToList();
                    var getusertotalbiohours = dbcontext.gettodaybiometrichoursbyusername(companyid, userId).ToList();
                    if (getusertotalhours[0] != null)
                    {
                        int todayuserhours = getusertotalhours[0].Value.Hours;
                        if (todayuserhours > 12)
                        {
                            todayuserhours = todayuserhours - 12;
                        }
                        if (todayuserhours == 0)
                        {
                            todayuserhours = Convert.ToInt32("00");
                        }
                        var minutes = getusertotalhours[0].Value.Minutes;
                        var seconds = getusertotalhours[0].Value.Seconds;
                        gettotaltodayhours = todayuserhours + ":" + minutes + ":" + seconds;
                    }

                    //var getusertotalhours = dbcontext.sp_gettotalhoursbyusername(companyid, username).ToList();
                    //var getusertotalproject = dbcontext.sp_gettotalprojectbyusername(companyid, username).ToList();
                    //var getusertotaltasks = dbcontext.sp_getotalTasksbyUsername(companyid, username).ToList();
                    //var getusertotalbiohours = dbcontext.gettotalbiometrichoursbyUserId(companyid, userId).ToList();

                    //if (gettodayhours[0] != null)
                    //{
                    //    string todayhours = gettodayhours[0];
                    //    gettotaltodayhours = todayhours;
                    //    if (todayhours > 12)
                    //    {
                    //        todayhours = todayhours - 12;
                    //    }
                    //    if (todayhours == 0)
                    //    {
                    //        todayhours = Convert.ToInt32("00");
                    //    }
                    //    var minutes = gettodayhours[0].Value.Minutes;
                    //    var seconds = gettodayhours[0].Value.Seconds;
                    //    gettotaltodayhours = todayhours + ":" + minutes + ":" + seconds;

                    //}
                    userlist.Add(new proclass { totaluserhours = gettotaltodayhours, totaluserproject = getusertotalproject, totalusertask = getusertotaltasks, totaluserbiohours = getusertotalbiohours });
                    return Json(userlist, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var gettodayhours = dbcontext.sp_gettodayhours(companyid).ToList();
                    var gettodayproject = dbcontext.sp_gettodayproject(companyid).ToList();
                    var gettodayTasks = dbcontext.sp_gettodayTasks(companyid).ToList();
                    var gettodayBiohours = dbcontext.gettodaybiometrichours(companyid).ToList();
                    var getidletime = dbcontext.sp_getidletime().ToList();
                    if (gettodayhours[0] != null)
                    {
                        int todayhours = gettodayhours[0].Value.Hours;
                        //if (todayhours > 12)
                        //{
                        //    todayhours = todayhours - 12;
                        //}
                        //if (todayhours == 0)
                        //{
                        //    todayhours = Convert.ToInt32("00");
                        //}

                        var minutes = gettodayhours[0].Value.Minutes;
                        var seconds = gettodayhours[0].Value.Seconds;
                        gettotaltodayhours = todayhours + ":" + minutes + ":" + seconds;
                    }
                    if (getidletime[0] != null)
                    {
                        int idlehours = getidletime[0].Value.Hours;
                        if (idlehours > 12)
                        {
                            idlehours = idlehours - 12;
                        }
                        if (idlehours == 0)
                        {
                            idlehours = Convert.ToInt32("00");
                        }
                        var idleminutes = getidletime[0].Value.Minutes;
                        var idleseconds = getidletime[0].Value.Seconds;
                        gettotalIdlehours = idlehours + ":" + idleminutes + ":" + idleseconds;

                    }
                    arrlist.Add(new proclass
                    {
                        todayhours = gettotaltodayhours,
                        idlehours = gettotalIdlehours,
                        todayproject = gettodayproject,
                        todaytasks = gettodayTasks,
                        Todaybiomatric = gettodayBiohours
                    });
                    return Json(arrlist, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult GetTimeLine()
        {

            var todowork = dbcontext.sp_gettodaywork().ToList();
            ArrayList arr = new ArrayList();

            var dictionary = new Dictionary<string, object>();
            var standardhours = "";



            for (int i = 0; i < todowork.Count(); i++)
            {
                var militaryhours = todowork[i].TodayDate.Value.TimeOfDay.Hours;
                var militarytime = todowork[i].Total_time.Value.Hours;
                if (militaryhours > 12)
                {
                    standardhours = Convert.ToString(militaryhours - 12);
                    standardhours = "0" + standardhours;
                }
                else
                {
                    standardhours = Convert.ToString(militaryhours);
                }
                //if(militarytime > 12)
                //{

                //    time = Convert.ToString(militarytime - 12);
                //    time = "0" + time;
                //}
                //else if (militarytime == 0)
                //{

                //    time = "00" + time;
                //}
                //else
                //{
                //    time = Convert.ToString(time);
                //}
                string minutes = Convert.ToString(todowork[i].TodayDate.Value.TimeOfDay.Minutes);
                string t_minutes = Convert.ToString(todowork[i].Total_time.Value.Minutes);
                if (Convert.ToInt32(minutes) < 9)
                {
                    minutes = "0" + minutes;
                }
                //if (Convert.ToInt32(t_minutes) < 9)
                //{
                //    t_minutes = "0" + t_minutes;
                //}
                string seconds = Convert.ToString(todowork[i].TodayDate.Value.TimeOfDay.Seconds);
                string t_seconds = Convert.ToString(todowork[i].Total_time.Value.Seconds);
                if (Convert.ToInt32(seconds) < 9)
                {
                    seconds = "0" + seconds;
                }
                var totaltime = militarytime + ":" + t_minutes + ":" + t_seconds;
                var standardtime = standardhours + ":" + minutes + ":" + seconds;

                // dictionary.Add("x:",standardtime);
                arrlist.Add(new proclass { Today_date = standardtime, Total_time = totaltime });
            }

            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Usertotalhours(string fromdate, string todate, string datalist = null, string username = null, string report = null)
        {
            GetOffices();
            SetLogo();
            string from_date = "";
            string to_date = "";
            if (!string.IsNullOrEmpty(username))
            {
                ViewBag.username = username;
            }
            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
            {
                DateTime dt_fromdate = Convert.ToDateTime(fromdate);
                DateTime dt_todate = Convert.ToDateTime(todate);
                from_date = String.Format("{0:yyyy-MM-dd}", dt_fromdate);
                to_date = String.Format("{0:yyyy-MM-dd}", dt_todate);
            }

            string[] splitstring = null;
            if (report != null)
            {
                splitstring = report.Split('(', ')');
            }
            int companyId = Convert.ToInt32(Session["company-id"]);
            if (datalist != null && datalist == "Today Report")
            {
                var hoursdatalist = dbcontext.sp_gettodayhoursdatalist(companyId).ToList();
                Session["fiteredtodaydatalist"] = hoursdatalist;
                return View("Usertotalhours", null);
            }
            else if (datalist != null && datalist == "Yesterday Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var dayofpreviousdate = 0;
                var month = 0;
                if (dayofnowdate == 1)
                {
                    dayofpreviousdate = 30 - 1;
                    month = Nowdate.Date.Month;
                    month = month - 1;
                }
                else
                {
                    dayofpreviousdate = dayofnowdate - 1;
                    month = Nowdate.Date.Month;
                }
                var year = Nowdate.Date.Year;
                var day = dayofpreviousdate;
                var Enddate = year + "-" + month + "-" + day;
                var hoursdatalist = dbcontext.sp_getyesterdayhoursdatalist(companyId, startdate, Enddate).ToList();
                Session["fiteredyesterdaydatalist"] = hoursdatalist;
                return View("Usertotalhours", null);
            }
            else if (datalist != null && datalist == "Past 7 days Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var dayofpreviousdate = 0;
                var month = 0;
                if (dayofnowdate <= 7)
                {

                    dayofpreviousdate = 30 - 7;
                    month = Nowdate.Date.Month;
                    month = month - 1;
                }
                else
                {
                    dayofpreviousdate = dayofnowdate - 7;
                    month = Nowdate.Date.Month;
                }

                var year = Nowdate.Date.Year;

                var day = dayofpreviousdate;
                var Enddate = year + "-" + month + "-" + day;
                var hoursdatalist = dbcontext.sp_getpast7dayshoursdatalist(companyId, startdate, Enddate).ToList();
                Session["fitered7daysdatalist"] = hoursdatalist;
                return View("Usertotalhours", null);


            }
            else if (datalist != null && datalist == "Past 30 days Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var monthofnowdate = Nowdate.Date.Month;
                var monthofpreviousdate = monthofnowdate - 1;
                var year = Nowdate.Date.Year;
                var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                var hoursdatalist = dbcontext.sp_getpast30dayshoursdatalist(companyId, startdate, Enddate).ToList();
                Session["fitered30daysdatalist"] = hoursdatalist;
                return View("Usertotalhours", null);
            }
            else if (datalist != null && datalist == "Date Range Report")
            {
                var hoursdatalist = dbcontext.sp_getdaterangehoursdatalist(companyId, from_date, to_date).ToList();
                Session["fitereddaterangedatalist"] = hoursdatalist;
                return View("Usertotalhours", null);
            }
            else
            {
                if (!string.IsNullOrEmpty(username) && splitstring[0].ToString() == "Today Report")
                {
                    // username = Session["username"].ToString();
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userhourslist = dbcontext.sp_gettodayhourslistbyusername(companyid, username).ToList();
                    Session["fiteredtodaydatalistbyusername"] = userhourslist;
                    return View("Usertotalhours", null);
                }
                else if (!string.IsNullOrEmpty(username) && splitstring[0].ToString() == "Yesterday Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = 0;
                    var month = 0;
                    if (dayofnowdate == 1)
                    {
                        dayofpreviousdate = 30 - 1;
                        month = Nowdate.Date.Month;
                        month = month - 1;
                    }
                    else
                    {
                        dayofpreviousdate = dayofnowdate - 1;
                        month = Nowdate.Date.Month;
                    }

                    var year = Nowdate.Date.Year;

                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    // username = Session["username"].ToString();
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userhourslist = dbcontext.sp_getyesterdayhourslistbyusername(companyid, username, startdate, Enddate).ToList();
                    Session["fiteredyesterdaydatalistbyusername"] = userhourslist;
                    return View("Usertotalhours", null);
                }
                else if (!string.IsNullOrEmpty(username) && splitstring[0] == "Past 7 days Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = 0;
                    var month = 0;
                    if (dayofnowdate <= 7)
                    {

                        dayofpreviousdate = 30 - 7;
                        month = Nowdate.Date.Month;
                        month = month - 1;
                    }
                    else
                    {
                        dayofpreviousdate = dayofnowdate - 7;
                        month = Nowdate.Date.Month;
                    }

                    var year = Nowdate.Date.Year;

                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    // username = Session["username"].ToString();
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userhourslist = dbcontext.sp_getpast7dayshourslistbyusername(companyid, username, startdate, Enddate).ToList();
                    Session["fiteredpast7daysdatalistbyusername"] = userhourslist;
                    return View("Usertotalhours", null);
                }
                else if (!string.IsNullOrEmpty(username) && splitstring[0] == "Past 30 days Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var monthofnowdate = Nowdate.Date.Month;
                    var monthofpreviousdate = monthofnowdate - 1;
                    var year = Nowdate.Date.Year;
                    var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                    // username = Session["username"].ToString();
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userhourslist = dbcontext.sp_getpast30dayshourslistbyusername(companyid, username, startdate, Enddate).ToList();
                    Session["fiteredpast30daysdatalistbyusername"] = userhourslist;
                    return View("Usertotalhours", null);
                }
                else if (!string.IsNullOrEmpty(username) && splitstring[0] == "Date Range Report")
                {
                    var hoursdatalist = dbcontext.sp_getdaterangehoursdatalistbyusername(companyId, username, from_date, to_date).ToList();
                    Session["fitereddaterangedatalistbyusername"] = hoursdatalist;
                    return View("Usertotalhours", null);
                }
                return View("Usertotalhours", null);
            }
        }

        [HttpGet]
        public ActionResult Usertotalprojects(string fromdate, string todate, string datalist = null, string username = null, string report = null)
        {
            GetOffices();
            SetLogo();
            int companyId = (Convert.ToInt32(Session["company-id"]));
            string from_date = "";
            string to_date = "";
            if (!string.IsNullOrEmpty(username))
            {
                ViewBag.username = username;
            }
            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
            {
                DateTime dt_fromdate = Convert.ToDateTime(fromdate);
                DateTime dt_todate = Convert.ToDateTime(todate);
                from_date = String.Format("{0:yyyy-MM-dd}", dt_fromdate);
                to_date = String.Format("{0:yyyy-MM-dd}", dt_todate);
            }

            string[] splitstring = null;
            if (report != null)
            {
                splitstring = report.Split('(', ')');
            }
            if (datalist != null && datalist == "Today Report")
            {

                var projectdatalist = dbcontext.sp_gettodayprojectsdatalist(companyId).ToList();
                Session["fiteredtodayprojectdatalist"] = projectdatalist;
                return View("Usertotalprojects");
            }
            else if (datalist != null && datalist == "Yesterday Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var dayofpreviousdate = 0;
                var month = 0;
                if (dayofnowdate == 1)
                {
                    dayofpreviousdate = 30 - 1;
                    month = Nowdate.Date.Month;
                    month = month - 1;
                }
                else
                {
                    dayofpreviousdate = dayofnowdate - 1;
                    month = Nowdate.Date.Month;
                }

                var year = Nowdate.Date.Year;

                var day = dayofpreviousdate;
                var Enddate = year + "-" + month + "-" + day;
                var projectdatalist = dbcontext.sp_getyesterdayprojectsdatalist(companyId, startdate, Enddate).ToList();
                Session["fiteredyesterdayprojectdatalist"] = projectdatalist;
                return View("Usertotalprojects");
            }
            else if (datalist != null && datalist == "Past 7 days Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var dayofpreviousdate = 0;
                var month = 0;
                if (dayofnowdate <= 7)
                {

                    dayofpreviousdate = 30 - 7;
                    month = Nowdate.Date.Month;
                    month = month - 1;
                }
                else
                {
                    dayofpreviousdate = dayofnowdate - 7;
                    month = Nowdate.Date.Month;
                }

                var year = Nowdate.Date.Year;

                var day = dayofpreviousdate;
                var Enddate = year + "-" + month + "-" + day;
                var projectdatalist = dbcontext.sp_getpast7dayprojectsdatalist(companyId, startdate, Enddate).ToList();
                Session["fitered7daysprojectdatalist"] = projectdatalist;
                return View("Usertotalprojects");

            }
            else if (datalist != null && datalist == "Past 30 days Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var monthofnowdate = Nowdate.Date.Month;
                var monthofpreviousdate = monthofnowdate - 1;
                var year = Nowdate.Date.Year;
                var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                var projectdatalist = dbcontext.sp_getpast30dayprojectsdatalist(companyId, startdate, Enddate).ToList();
                Session["fitered30daysprojectdatalist"] = projectdatalist;
                return View("Usertotalprojects");
            }
            else if (datalist != null && datalist == "Date Range Report")
            {
                var projectdatalist = dbcontext.sp_getdaterangeprojectsdatalist(companyId, from_date, to_date).ToList();
                Session["fitereddaterangeprojectdatalist"] = projectdatalist;
                return View("Usertotalprojects");
            }

            else
            {
                if (username != null && splitstring[0] == "Today Report")
                {

                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userprojectlist = dbcontext.sp_gettodayprojectslistbyusername(companyid, username).ToList();
                    Session["fiteredtodayprojectdatalistbyusername"] = userprojectlist;
                    return View("Usertotalprojects");
                }
                else if (username != null && splitstring[0] == "Yesterday Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = 0;
                    var month = 0;
                    if (dayofnowdate == 1)
                    {
                        dayofpreviousdate = 30 - 1;
                        month = Nowdate.Date.Month;
                        month = month - 1;
                    }
                    else
                    {
                        dayofpreviousdate = dayofnowdate - 1;
                        month = Nowdate.Date.Month;
                    }

                    var year = Nowdate.Date.Year;

                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    var userprojectlist = dbcontext.sp_getyesterdayprojectslistbyusername(companyId, username, startdate, Enddate).ToList();
                    Session["fiteredyesterdayprojectdatalistbyusername"] = userprojectlist;
                    return View("Usertotalprojects");
                }
                else if (username != null && splitstring[0] == "Past 7 days Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = 0;
                    var month = 0;
                    if (dayofnowdate <= 7)
                    {

                        dayofpreviousdate = 30 - 7;
                        month = Nowdate.Date.Month;
                        month = month - 1;
                    }
                    else
                    {
                        dayofpreviousdate = dayofnowdate - 7;
                        month = Nowdate.Date.Month;
                    }

                    var year = Nowdate.Date.Year;

                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    var userprojectlist = dbcontext.sp_getpast7dayprojectslistbyusername(companyId, username, startdate, Enddate).ToList();
                    Session["fiteredpast7daysprojectdatalistbyusername"] = userprojectlist;
                    return View("Usertotalprojects");

                }
                else if (username != null && splitstring[0] == "Past 30 days Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var monthofnowdate = Nowdate.Date.Month;
                    var monthofpreviousdate = monthofnowdate - 1;
                    var year = Nowdate.Date.Year;
                    var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                    var userprojectlist = dbcontext.sp_getpast30dayprojectslistbyusername(companyId, username, startdate, Enddate).ToList();
                    Session["fiteredpast30daysprojectdatalistbyusername"] = userprojectlist;
                    return View("Usertotalprojects");
                }
                else if (username != null && splitstring[0] == "Date Range Report")
                {
                    var userprojectlist = dbcontext.sp_daterangeprojectslistbyusername(companyId, username, from_date, to_date).ToList();
                    Session["fitereddaterangeprojectdatalistbyusername"] = userprojectlist;
                    return View("Usertotalprojects");
                }
            }
            return View("Usertotalprojects");
        }

        [HttpGet]
        public ActionResult Usertotalbiohours(string fromdate, string todate, string datalist = null, string username = null, string report = null)
        {
            GetOffices();
            SetLogo();
            int companyId = (Convert.ToInt32(Session["company-id"]));
            string from_date = "";
            string to_date = "";
            if (!string.IsNullOrEmpty(username))
            {
                ViewBag.username = username;
            }
            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
            {
                DateTime dt_fromdate = Convert.ToDateTime(fromdate);
                DateTime dt_todate = Convert.ToDateTime(todate);
                from_date = String.Format("{0:yyyy-MM-dd}", dt_fromdate);
                to_date = String.Format("{0:yyyy-MM-dd}", dt_todate);
            }
            int userId = Convert.ToInt32(Session["userId"]);
            int companyid = Convert.ToInt32(Session["company-id"]);
            string[] splitstring = null;
            if (report != null)
            {
                splitstring = report.Split('(', ')');
            }
            if (datalist != null && datalist == "Today Report")
            {
                var biohoursdatalist = dbcontext.sp_gettodaybiohoursdatalist(companyId).ToList();
                Session["fiteredtodaybiohoursdatalist"] = biohoursdatalist;
                return View("Usertotalbiohours");
            }
            else if (datalist != null && datalist == "Yesterday Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var dayofpreviousdate = 0;
                var month = 0;
                if (dayofnowdate == 1)
                {
                    dayofpreviousdate = 30 - 1;
                    month = Nowdate.Date.Month;
                    month = month - 1;
                }
                else
                {
                    dayofpreviousdate = dayofnowdate - 1;
                    month = Nowdate.Date.Month;
                }

                var year = Nowdate.Date.Year;

                var day = dayofpreviousdate;
                var Enddate = year + "-" + month + "-" + day;
                var biohoursdatalist = dbcontext.sp_getyesterdaybiohoursdatalist(companyId, startdate, Enddate).ToList();
                Session["fiteredyesterdaybiohoursdatalist"] = biohoursdatalist;
                return View("Usertotalbiohours");
            }
            else if (datalist != null && datalist == "Past 7 days Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var dayofpreviousdate = 0;
                var month = 0;
                if (dayofnowdate <= 7)
                {

                    dayofpreviousdate = 30 - 7;
                    month = Nowdate.Date.Month;
                    month = month - 1;
                }
                else
                {
                    dayofpreviousdate = dayofnowdate - 7;
                    month = Nowdate.Date.Month;
                }
                var year = Nowdate.Date.Year;
                var day = dayofpreviousdate;
                var Enddate = year + "-" + month + "-" + day;
                var biohoursdatalist = dbcontext.sp_getpast7daysbiohoursdatalist(companyId, startdate, Enddate).ToList();
                Session["fitered7daysbiohoursdatalist"] = biohoursdatalist;
                return View("Usertotalbiohours");

            }
            else if (datalist != null && datalist == "Past 30 days Report")
            {
                var Nowdate = DateTime.Now.Date;
                var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                var dayofnowdate = Nowdate.Date.Day;
                var monthofnowdate = Nowdate.Date.Month;
                var monthofpreviousdate = monthofnowdate - 1;
                var year = Nowdate.Date.Year;
                var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                var biohoursdatalist = dbcontext.sp_getpast30daysbiohoursdatalist(companyId, startdate, Enddate).ToList();
                Session["fitered30daysbiohoursdatalist"] = biohoursdatalist;
                return View("Usertotalbiohours");
            }
            else if (datalist != null && datalist == "Date Range Report")
            {
                var biohoursdatalist = dbcontext.sp_getdaterangesbiohoursdatalist(companyId, from_date, to_date).ToList();
                Session["fitereddaterangebiohoursdatalist"] = biohoursdatalist;
                return View("Usertotalbiohours");
            }
            else
            {
                if (username != null && splitstring[0] == "Today Report")
                {
                    var Usertotalbiohours = dbcontext.sp_gettodaybiohoursdatalistbyusername(companyid, userId).ToList();
                    Session["fiteredtodaybiohoursdatalistbyusername"] = Usertotalbiohours;
                    return View("Usertotalbiohours");
                }
                else if (username != null && splitstring[0] == "Yesterday Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = 0;
                    var month = 0;
                    if (dayofnowdate == 1)
                    {
                        dayofpreviousdate = 30 - 1;
                        month = Nowdate.Date.Month;
                        month = month - 1;
                    }
                    else
                    {
                        dayofpreviousdate = dayofnowdate - 1;
                        month = Nowdate.Date.Month;
                    }
                    var year = Nowdate.Date.Year;
                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    var Usertotalbiohours = dbcontext.sp_getyesterdaybiohoursdatalistbyusername(companyId, userId, startdate, Enddate).ToList();
                    Session["fiteredyesterdaybiohoursdatalistbyusername"] = Usertotalbiohours;
                    return View("Usertotalbiohours");
                }
                else if (username != null && splitstring[0] == "Past 7 days Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = 0;
                    var month = 0;
                    if (dayofnowdate <= 7)
                    {

                        dayofpreviousdate = 30 - 7;
                        month = Nowdate.Date.Month;
                        month = month - 1;
                    }
                    else
                    {
                        dayofpreviousdate = dayofnowdate - 7;
                        month = Nowdate.Date.Month;
                    }

                    var year = Nowdate.Date.Year;

                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    var Usertotalbiohours = dbcontext.sp_getpast7daysbiohoursdatalistbyusername(companyId, userId, startdate, Enddate).ToList();
                    Session["fiteredpast7daysbiohoursdatalistbyusername"] = Usertotalbiohours;
                    return View("Usertotalbiohours");

                }
                else if (username != null && splitstring[0] == "Past 30 days Report")
                {
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var monthofnowdate = Nowdate.Date.Month;
                    var monthofpreviousdate = monthofnowdate - 1;
                    var year = Nowdate.Date.Year;
                    var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                    var Usertotalbiohours = dbcontext.sp_getpast30daysbiohoursdatalistbyusername(companyId, userId, startdate, Enddate).ToList();
                    Session["fiteredpast30daysbiohoursdatalistbyusername"] = Usertotalbiohours;
                    return View("Usertotalbiohours");
                }
                else if (username != null && splitstring[0] == "Date Range Report")
                {
                    var Usertotalbiohours = dbcontext.sp_daterangesbiohoursdatalistbyusername(companyId, userId, from_date, to_date).ToList();
                    Session["fitereddaterangebiohoursdatalistbyusername"] = Usertotalbiohours;
                    return View("Usertotalbiohours");
                }
            }
            return View("Usertotalbiohours");

        }
        public ActionResult _30daysfilter(string _30dayfiltertext, string fullurl = null)
        {
            if (fullurl != null && _30dayfiltertext == "Past 30 days")
            {
                //    Uri url = new Uri(System.Web.HttpContext.Current.Request.Url);
                //    string path = String.Format("{0}{1}{2}{3}", url.Scheme, Uri.SchemeDelimiter, url.Authority, url.AbsolutePath);
                if (fullurl.ToString() == fullurl)
                {
                    int companyId = (Convert.ToInt32(Session["company-id"]));
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var monthofnowdate = Nowdate.Date.Month;
                    var monthofpreviousdate = monthofnowdate - 1;
                    var year = Nowdate.Date.Year;
                    var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
                    GetOffices();
                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userprojectlist = dbcontext.sp_getpast30dayprojectsdatalist(companyid, startdate, Enddate).ToList();
                    return View("Usertotalprojects", userprojectlist);
                }
            }
            return View();
        }
        public ActionResult _7daysfilter(string _7dayfiltertext, string fullurl = null)
        {
            if (fullurl != null && _7dayfiltertext == "Past 7 days")
            {
                Uri url = new Uri(Request.Url.AbsoluteUri);
                string path = String.Format("{0}{1}{2}{3}", url.Scheme, Uri.SchemeDelimiter, url.Authority, url.AbsolutePath);
                if (fullurl == path)
                {
                    int companyId = (Convert.ToInt32(Session["company-id"]));
                    var Nowdate = DateTime.Now.Date;
                    var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
                    var dayofnowdate = Nowdate.Date.Day;
                    var dayofpreviousdate = dayofnowdate - 7;
                    var year = Nowdate.Date.Year;
                    var month = Nowdate.Date.Month;
                    var day = dayofpreviousdate;
                    var Enddate = year + "-" + month + "-" + day;
                    GetOffices();

                    int companyid = Convert.ToInt32(Session["company-id"]);
                    var userprojectlist = dbcontext.sp_getpast7daysprojectslist(companyid, startdate, Enddate).ToList();

                    return View("Usertotalprojects", userprojectlist);
                }
            }
            return View();
        }

        List<proclass> yesterdaylist = new List<proclass>();
        public ActionResult GetYesterdayReport(string usertext = null)
        {
            int Userid = 0;
            int companyId = (Convert.ToInt32(Session["company-id"]));
            var Nowdate = DateTime.Now.Date;
            var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            var dayofnowdate = Nowdate.Date.Day;
            var dayofpreviousdate = 0;
            var month = 0;
            if (dayofnowdate == 1)
            {
                dayofpreviousdate = 30 - 1;
                month = Nowdate.Date.Month;
                month = month - 1;
            }
            else
            {
                dayofpreviousdate = dayofnowdate - 1;
                month = Nowdate.Date.Month;
            }
            var year = Nowdate.Date.Year;
            var day = dayofpreviousdate;
            var Enddate = year + "-" + month + "-" + day;
            if (usertext != null)
            {
                Userid = GetidbyUsername(usertext);
            }
            if (usertext != null)
            {
                var yesterdayshoursbyusername = dbcontext.sp_yesterdayhoursbyusername(companyId, startdate, Enddate, usertext).ToList();
                var yesterdaysprojectsbyusername = dbcontext.sp_yesterdaysprojectbyusername(companyId, startdate, Enddate, usertext).ToList();
                var yesterdaystasksbyusername = dbcontext.sp_yesterdaystasksbyusername(companyId, startdate, Enddate, usertext).ToList();
                var yesterdaybiomatrichoursbyusername = dbcontext.sp_yesterdaysbiomatrichoursbyusername(companyId, startdate, Enddate, Userid).ToList();
                yesterdaylist.Add(new proclass
                {
                    yesterdayhour = yesterdayshoursbyusername[0],
                    yesterdayproject = yesterdaysprojectsbyusername[0],
                    yesterdaytask = yesterdaystasksbyusername[0],
                    yesterdaybiohours = yesterdaybiomatrichoursbyusername[0],

                });
                return Json(yesterdaylist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var yesterdayshours = dbcontext.sp_yesterdayhours(companyId, startdate, Enddate).ToList();
                var yesterdaysproject = dbcontext.sp_yesterdayproject(companyId, startdate, Enddate).ToList();
                var yesterdaystasks = dbcontext.sp_yesterdaytasks(companyId, startdate, Enddate).ToList();
                var yesterdaysbiohours = dbcontext.sp_yesterdaybiomatrichours(companyId, startdate, Enddate).ToList();

                if (yesterdayshours[0] == null)
                {
                    string yesterdaynullvalue = Convert.ToString("0");
                    yesterdaylist.Add(new proclass
                    {
                        yesterdayhour = yesterdaynullvalue,
                        yesterdayproject = yesterdaysproject[0],
                        yesterdaytask = yesterdaystasks[0],
                        yesterdaybiohours = yesterdaysbiohours[0],

                    });
                }
                else
                {
                    yesterdaylist.Add(new proclass
                    {
                        yesterdayhour = yesterdayshours[0],
                        yesterdayproject = yesterdaysproject[0],
                        yesterdaytask = yesterdaystasks[0],
                        yesterdaybiohours = yesterdaysbiohours[0],

                    });
                }


                return Json(yesterdaylist, JsonRequestBehavior.AllowGet);
            }

        }

        List<proclass> _7daysreportlist = new List<proclass>();
        List<proclass> _7daysreportlistofusers = new List<proclass>();
        public ActionResult Past7daysreport(string usertext = null)
        {
            int Userid = 0;
            int companyId = (Convert.ToInt32(Session["company-id"]));
            var Nowdate = DateTime.Now.Date;
            var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            var dayofnowdate = Nowdate.Date.Day;
            var dayofpreviousdate = 0;
            var month = 0;
            if (dayofnowdate <= 7)
            {

                dayofpreviousdate = 30 - 7;
                month = Nowdate.Date.Month;
                month = month - 1;
            }
            else
            {
                dayofpreviousdate = dayofnowdate - 7;
                month = Nowdate.Date.Month;
            }
            var year = Nowdate.Date.Year;
            var day = dayofpreviousdate;

            var Enddate = year + "-" + month + "-" + day;
            if (usertext != null)
            {
                Userid = GetidbyUsername(usertext);
            }
            if (usertext != null)
            {
                var past7dayshoursofuser = dbcontext.sp_past7dayshoursbyusername(companyId, startdate, Enddate, usertext).ToList();
                var past7daysprojectsofuser = dbcontext.sp_past7daysprojectbyusername(companyId, startdate, Enddate, usertext).ToList();
                var past7daystasksofuser = dbcontext.sp_7daystasksbyusername(companyId, startdate, Enddate, usertext).ToList();
                var past7daysbiohoursofuser = dbcontext.sp_7daysbiomatrichoursbyuserna(companyId, startdate, Enddate, Userid).ToList();
                _7daysreportlistofusers.Add(new proclass
                {
                    _7dayshoursofuser = past7dayshoursofuser[0],
                    _7dayprojectofuser = past7daysprojectsofuser[0],
                    _7daytasksofuser = past7daystasksofuser[0],
                    _7daybiohoursofuser = past7daysbiohoursofuser[0]
                });
                return Json(_7daysreportlistofusers, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var past7dayshours = dbcontext.sp_past7dayshours(companyId, startdate, Enddate).ToList();
                var past7daysproject = dbcontext.sp_past7daysproject(companyId, startdate, Enddate).ToList();
                var past7daystasks = dbcontext.sp_7daystasks(companyId, startdate, Enddate).ToList();
                var past7daybiohours = dbcontext.sp_7daysbiomatrichours(companyId, startdate, Enddate).ToList();
                _7daysreportlist.Add(new proclass
                {
                    _7dayshours = past7dayshours[0],
                    _7dayproject = past7daysproject[0],
                    _7daytasks = past7daystasks[0],
                    _7daybiohours = past7daybiohours[0]
                });
                return Json(_7daysreportlist, JsonRequestBehavior.AllowGet);
            }
        }
        List<proclass> _30dayslist = new List<proclass>();
        public ActionResult Get30daysReport(string usertext = null)
        {
            int Userid = 0;
            int companyId = (Convert.ToInt32(Session["company-id"]));
            var Nowdate = DateTime.Now.Date;
            var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            var dayofnowdate = Nowdate.Date.Day;
            var monthofnowdate = Nowdate.Date.Month;
            var monthofpreviousdate = monthofnowdate - 1;
            var year = Nowdate.Date.Year;
            var Enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
            if (usertext != null)
            {
                Userid = GetidbyUsername(usertext);
            }
            if (usertext != null)
            {
                var past30dayshoursbyusername = dbcontext.sp_past30dayshoursbyusername(companyId, startdate, Enddate, usertext).ToList();
                var past30daysprojectsbyusername = dbcontext.sp_past30daysprojectbyusername(companyId, startdate, Enddate, usertext).ToList();
                var past30daystasksbyusername = dbcontext.sp_30daystasksbyusername(companyId, startdate, Enddate, usertext).ToList();
                var past30daysbiohoursbyusername = dbcontext.sp_30daybiomatrichoursbyusername(companyId, startdate, Enddate, Userid).ToList();
                _30dayslist.Add(new proclass
                {
                    _30dayshours = past30dayshoursbyusername[0],
                    _30dayprojects = past30daysprojectsbyusername[0],
                    _30daytasks = past30daystasksbyusername[0],
                    _30daybiohours = past30daysbiohoursbyusername[0],
                });
                return Json(_30dayslist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var past30dayshours = dbcontext.sp_past30dayshours(companyId, startdate, Enddate).ToList();
                var past30daysprojects = dbcontext.sp_past30daysproject(companyId, startdate, Enddate).ToList();
                var past30daystasks = dbcontext.sp_30daystasks(companyId, startdate, Enddate).ToList();
                var past30daysbiohours = dbcontext.sp_30daysbiomatrichours(companyId, startdate, Enddate).ToList();
                _30dayslist.Add(new proclass
                {
                    _30dayshours = past30dayshours[0],
                    _30dayprojects = past30daysprojects[0],
                    _30daytasks = past30daystasks[0],
                    _30daybiohours = past30daysbiohours[0],
                });

                return Json(_30dayslist, JsonRequestBehavior.AllowGet);
            }

        }

        List<proclass> daterangelist = new List<proclass>();
        public ActionResult daterange(string fromdate, string todate, string usertext = null)
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            int Userid = 0;
            string from_date = "";
            string to_date = "";
            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
            {
                DateTime dt_fromdate = Convert.ToDateTime(fromdate);
                DateTime dt_todate = Convert.ToDateTime(todate);
                from_date = String.Format("{0:yyyy-MM-dd}", dt_fromdate);
                to_date = String.Format("{0:yyyy-MM-dd}", dt_todate);
            }
            if (usertext != null)
            {
                Userid = GetidbyUsername(usertext);
            }
            if (!string.IsNullOrEmpty(usertext))
            {
                var getrangehours = dbcontext.sp_getdaterangehoursbyusername(companyid, from_date, to_date, usertext).ToList();
                var getrangeprojects = dbcontext.sp_getdaterangesprojectbyusername(companyid, from_date, to_date, usertext).ToList();
                var getrangetasks = dbcontext.sp_getdaterangetasksbyusername(companyid, from_date, to_date, usertext).ToList();
                var getrangebiomatrichours = dbcontext.sp_getdaterangebiomatrichoursbyusername(companyid, from_date, to_date, Userid).ToList();
                daterangelist.Add(new proclass
                {
                    daterangehoursbyusername = getrangehours,
                    daterangeprojectsbyusername = getrangeprojects,
                    daterangetasksbyusername = getrangetasks,
                    daterangebiohoursbyusername = getrangebiomatrichours
                });
            }
            else
            {
                var getrangehours = dbcontext.sp_getdaterangehours(companyid, from_date, to_date).ToList();
                var getrangeprojects = dbcontext.sp_daterangeproject(companyid, from_date, to_date).ToList();
                var getrangetasks = dbcontext.sp_daterangetasks(companyid, from_date, to_date).ToList();
                var getrangebiomatrichours = dbcontext.sp_daterangebiomatrichours(companyid, from_date, to_date).ToList();
                daterangelist.Add(new proclass
                {
                    daterangehours = getrangehours,
                    daterangeprojects = getrangeprojects,
                    daterangetasks = getrangetasks,
                    daterangebiohours = getrangebiomatrichours
                });
            }

            return Json(daterangelist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddUser(Usermetadata user)
        {
            SetLogo();
            if (ModelState.IsValid)
            {
                int companyid = Convert.ToInt32(Session["company-id"]);
                if (Session["updateval"] != null)
                {
                    USER obj = new USER();
                    GetOffices();
                    GetCompanies(companyid);
                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var image = System.Web.HttpContext.Current.Request.Files["file"];
                        if (image.ContentLength > 0)
                        {
                            var imageName = Path.GetFileName(image.FileName);
                            var ext = Path.GetExtension(image.FileName);

                            // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
                            var comPath = Server.MapPath("~/AdminAssets/Images/") + imageName;
                            image.SaveAs(comPath);
                            obj.UserImage = imageName;

                        }
                    }

                    obj.Id = Convert.ToInt32(Session["updateval"]);
                    obj.Username = user.Username;
                    obj.Mobile = user.Mobile;
                    obj.Password = user.Password;
                    obj.Gender = user.Gender;
                    obj.Email = user.Email;

                    obj.CompanyID = companyid;
                    obj.OfficeID = user.OfficeID;
                    dbcontext.sp_updateusers(obj.Id, obj.Username, obj.Mobile, obj.Password, obj.Gender, obj.Email, obj.UserImage, obj.OfficeID);
                    dbcontext.SaveChanges();
                    Session.Remove("updateval");
                    var allusers = dbcontext.USERS.Where(x => x.Id == obj.Id).ToList();
                    return Json(new { data = allusers, text = "Update" });
                }
                else if(Session["UserRole"] != null && Session["UserRole"].ToString() == "Admin")
                {
                    USER obj = new USER();
                    GetOffices();
                    GetCompanies(companyid);
                    if (IsExsistUser(user.Username, user.Email))
                    {
                        return Json(new { text = "Exsist" });
                    }
                    else 
                    {
                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            var image = System.Web.HttpContext.Current.Request.Files["file"];
                            if (image.ContentLength > 0)
                            {
                                var imageName = Path.GetFileName(image.FileName);
                                var ext = Path.GetExtension(image.FileName);

                                // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
                                var comPath = Server.MapPath("~/AdminAssets/Images/") + imageName;
                                image.SaveAs(comPath);
                                obj.UserImage = imageName;

                            }
                        }
                        obj.Username = user.Username;
                        obj.Mobile = user.Mobile;
                        obj.Password = user.Password;
                        obj.Gender = user.Gender;
                        obj.Email = user.Email;
                        obj.CompanyID = companyid;
                        obj.OfficeID = user.OfficeID;
                        dbcontext.USERS.Add(obj);
                        dbcontext.SaveChanges();
                        var users = LoadUsersLastRecord();
                        return Json(new { data = users, text = "Add" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    USER obj = new USER();
                    tbl_UsersRole userrole = new tbl_UsersRole();
                    //GetOffices();
                    GetCompanies(null);
                    GetRoles();
                    if (IsExsistUser(user.Username, user.Email))
                    {
                        return Json(new { text = "Exsist" });
                    }
                    else
                    {
                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            var image = System.Web.HttpContext.Current.Request.Files["file"];
                            if (image.ContentLength > 0)
                            {
                                var imageName = Path.GetFileName(image.FileName);
                                var ext = Path.GetExtension(image.FileName);

                                // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
                                var comPath = Server.MapPath("~/AdminAssets/Images/") + imageName;
                                image.SaveAs(comPath);
                                obj.UserImage = imageName;
                            }
                        }
                        obj.Username = user.Username;
                        obj.Mobile = user.Mobile;
                        obj.Password = user.Password;
                        obj.Gender = user.Gender;
                        obj.Email = user.Email;
                        obj.CompanyID = user.CompanyID;
                        
                        //obj.OfficeID = user.OfficeID;
                        dbcontext.USERS.Add(obj);
                        dbcontext.SaveChanges();
                        var users = LoadUsersLastRecord();
                        userrole.RoleId = user.RoleID;
                        userrole.UserId = users[0].Id;
                        dbcontext.tbl_UsersRole.Add(userrole);
                        dbcontext.SaveChanges();
                        return Json(new { data = users, text = "AdminAdd" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json("InValid Model", JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsExsistUser(string UserName, string UserEmail)
        {
            var list = dbcontext.USERS.Where(x => x.Username == UserName || x.Email == UserEmail).ToList();
            if (list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<sp_getlastrecordfromusers_Result> LoadUsersLastRecord()
        {

            var users = dbcontext.sp_getlastrecordfromusers().ToList();
            return users;
        }

        public ActionResult ViewUsers(int? id, int? pageindex = 0, int? pagesize = 5, int? totalrows = 12)
        {
            SetLogo();
            //Old Code
            //string adminid = Session["adminid"].ToString();
            //if (Session["adminname"].ToString() == "SuperAdmin")
            //{
            //    var list = dbcontext.sp_ViewAllUsers(Convert.ToInt32(Session["company-id"]), id).ToList();
            //    if (list.Count > 0)
            //    {
            //        GetRolls();
            //        // Getbiomatricfeatures();
            //        GetCompanies(null);
            //        GetOffices();
            //        Session["ViewAllUsers"] = list;
            //        return PartialView("_ViewUser");
            //    }
            //    else
            //    {
            //        GetRolls();
            //        GetCompanies(null);
            //        //GetOffices();
            //        return PartialView("_ViewUser");
            //    }
            //}
            //else
            //{
            //    //GetRolls();
            //   // Getbiomatricfeatures();
            //    GetCompanies(Convert.ToInt32(Session["company-id"]));
            //    GetOffices();
            //    var list = dbcontext.sp_ViewUsers(adminid, Convert.ToInt32(Session["company-id"])).ToList();
            //    if (list.Count > 0)
            //    {
            //        return PartialView("_ViewUser", list);
            //    }
            //    else
            //    {
            //        ViewBag.msg = "No Records Found";
            //        // GetRolls();
            //        Getbiomatricfeatures();
            //        GetCompanies(Convert.ToInt32(Session["company-id"]));
            //        GetOffices();
            //        return PartialView("_ViewUser", list);
            //    }
            //}
            //End Old Code
            if (id != null)
            {
                var list = dbcontext.USERS.Where(x => x.Id == id).SingleOrDefault();
                Session["updateval"] = id;
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else if(Session["UserRole"].ToString() != "SuperAdmin")
            {
                //New Code

                int companyid = Convert.ToInt32(Session["company-id"]);
                var users = dbcontext.USERS.Where(x => x.CompanyID == companyid).ToList();
                //var users = dbcontext.sp_GetUserslist(pageindex, pagesize, totalrows, companyid).ToList();
                GetOffices();
                GetCompanies(companyid);
                return View(users);

                //End New Code
            }
            else
            {
                //New Code

                int companyid = Convert.ToInt32(Session["company-id"]);
                var users = dbcontext.USERS.Where(x => x.CompanyID == companyid).ToList();
                //var users = dbcontext.sp_GetUserslist(pageindex, pagesize, totalrows, companyid).ToList();
                GetOffices();
                GetCompanies(null);
                GetRoles();
                return View(users);

                //End New Code
            }
        }
        [HttpGet]
        public ActionResult DeleteUser(int? delid)
        {
            if (delid > 0)
            {

                USER obj = dbcontext.USERS.Where(x => x.Id == delid).SingleOrDefault();
                if (obj != null)
                {
                    dbcontext.USERS.Remove(obj);
                    dbcontext.SaveChanges();
                    return Json("Ok", JsonRequestBehavior.AllowGet);
                }
                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Not Ok", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetUsersRow()
        {
            var list = dbcontext.sp_getUserrows().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewOffice(int? id)
        {
            SetLogo();
            GetOffices();
            GetUsers();
            if (id != null)
            {
                var list = dbcontext.OfficeLocs.Where(x => x.Id == id).SingleOrDefault();
                Session["updateval"] = id;
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                int companyid = Convert.ToInt32(Session["company-id"]);
                if (companyid.ToString() != null)
                {
                    var list = dbcontext.OfficeLocs.Where(x => x.CompanyID == companyid).ToList();
                    var Hours = "20:20:19";
                    List<Office> officelist = new List<Office>();
                    foreach (var item in list)
                    {
                        List<Nullable<int>> userscount = GetUsersCountByOfficeId(item.Id);
                        officelist.Add(new Office
                        {
                            Id = item.Id,
                            OfficeImage_str = item.OfficeImage,
                            Name = item.Name,
                            Usercount = userscount,
                            Status = item.Status,
                            Hours = Hours
                        });
                    }

                    Session["officelist"] = officelist;
                    return View(officelist);
                }
                else
                {
                    return View();
                }
            }


            //else
            //{
            //    GetAdminsByID();
            //    //GetOffices();
            //    var list = dbcontext.OfficeLocs.Where(x => x.Additionalid == id && x.CompanyId == companyid).ToList();
            //    if (list.Count > 0)
            //    {
            //        return View("_viewoffice", list);
            //    }
            //    else
            //    {
            //        return View("_viewoffice");
            //    }
            //}
        }

        public List<Nullable<int>> GetUsersCountByOfficeId(int? Id)
        {
            int companyId = (Convert.ToInt32(Session["company-id"]));
            var list = dbcontext.sp_getUserscount(Id, companyId).ToList();
            return list;
        }
        [HttpGet]
        public ActionResult GetUsersByOfficeID(int officeid)
        {
            var userslist = dbcontext.USERS.Where(x => x.OfficeID == officeid).ToList();

            ViewBag.Userslist = userslist;
            return Json(new { data = userslist }, JsonRequestBehavior.AllowGet);
        }

        //Post method to perform insertion and updation

        [HttpPost]
        public ActionResult AddOffice(Office office)
        {
            SetLogo();
            GetOffices();
            List<Office> officelist = new List<Office>();
            if (ModelState.IsValid)
            {
                //    if (Session["adminname"].ToString() == "SuperAdmin")
                //    {
                //        GetAdminsByID();
                //        OfficeLoc obj = new OfficeLoc();
                //        obj.Name = office.Name;
                //        obj.Title = office.Title;
                //        obj.Notes = office.Notes;
                //        // obj.Additionalid = Convert.ToInt32(Session["additionalId"]);
                //        obj.Additionalid = office.Additionalid;
                //        obj.CompanyId = Convert.ToInt32(Session["company-id"]);
                //        obj.Status = office.Status;
                //        dbcontext.OfficeLocs.Add(obj);
                //        dbcontext.SaveChanges();
                //        TempData["msg"] = "Office Added Successfully";
                //        return RedirectToAction("AddOffice", "Home");
                //    }
                //    else
                //    {
                //GetAdminsByID();
                // GetUsersbyentity();



                if (Session["updateval"] != null)
                {
                    OfficeLoc obj = new OfficeLoc();
                    obj.Id = Convert.ToInt32(Session["updateval"]);

                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var image = System.Web.HttpContext.Current.Request.Files["file"];
                        if (image.ContentLength > 0)
                        {
                            var imageName = Path.GetFileName(image.FileName);
                            var ext = Path.GetExtension(image.FileName);

                            // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
                            var comPath = Server.MapPath("~/AdminAssets/Images/") + imageName;
                            image.SaveAs(comPath);
                            obj.OfficeImage = imageName;
                            var l = obj.OfficeImage.Length;
                        }
                    }
                    obj.Name = office.Name;
                    obj.Title = office.Title;
                    obj.Notes = office.Notes;
                    obj.Status = office.Status;
                    dbcontext.sp_updateOffices(obj.Id, obj.Name, obj.Title, obj.Notes, obj.Status, obj.OfficeImage);
                    dbcontext.SaveChanges();
                    Session.Remove("updateval");
                    var currentrow = LoadAllOffices();
                    var Hours = "20:20:19";
                    foreach (var item in currentrow)
                    {
                        List<Nullable<int>> userscount = GetUsersCountByOfficeId(item.Id);
                        officelist.Add(new Office
                        {
                            Id = item.Id,
                            OfficeImage_str = item.OfficeImage,
                            Name = item.Name,
                            Usercount = userscount,
                            Status = item.Status,
                            Hours = Hours
                        });
                    }
                    return Json(new { data = officelist, text = "Update" });
                }
                else
                {
                    OfficeLoc obj = new OfficeLoc();
                    //string Name = Path.GetFileName(office.OfficeImage.FileName);
                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var image = System.Web.HttpContext.Current.Request.Files["file"];
                        if (image.ContentLength > 0)
                        {
                            var imageName = Path.GetFileName(image.FileName);
                            var ext = Path.GetExtension(image.FileName);

                            // var comPath = Server.MapPath("~/AdminAssets/images/") + ImageName;
                            var comPath = Server.MapPath("~/AdminAssets/Images/") + imageName;
                            image.SaveAs(comPath);
                            obj.OfficeImage = imageName;
                            var l = obj.OfficeImage.Length;
                        }
                    }
                    //string Name = office.OfficeImage_str;
                    //string path = Server.MapPath("~/AdminAssets/Images");
                    //string fullpath = Path.Combine(path, Name);
                    //office.OfficeImage.SaveAs(fullpath);

                    obj.Name = office.Name;
                    obj.Title = office.Title;
                    obj.Notes = office.Notes;
                    obj.Status = office.Status;

                    obj.Additionalid = Convert.ToInt32(Session["additionalId"]);
                    obj.CompanyID = Convert.ToInt32(Session["company-id"]);
                    dbcontext.OfficeLocs.Add(obj);
                    dbcontext.SaveChanges();
                    var offices = LoadOfficeLastRecord();
                    var Hours = "20:20:19";
                    foreach (var item in offices)
                    {
                        List<Nullable<int>> userscount = GetUsersCountByOfficeId(item.Id);
                        officelist.Add(new Office
                        {
                            Id = item.Id,
                            OfficeImage_str = item.OfficeImage,
                            Name = item.Name,
                            Usercount = userscount,
                            Status = item.Status,
                            Hours = Hours
                        });
                    }
                    return Json(new { data = officelist, text = "Add" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                return Json("Model state is not valid");
            }
        }

        public List<sp_getlastrecordfromoffices_Result> LoadOfficeLastRecord()
        {
            var alloffices = dbcontext.sp_getlastrecordfromoffices().ToList();
            return alloffices;
        }

        [HttpGet]
        public ActionResult ScreenshotagainstTask(string Task = null)
        {
            GetOffices();
            SetLogo();
            var screenshotagainttask = dbcontext.Screenshots.Where(x => x.Task == Task).ToList();
            return View("ScreenshotagainstTask", screenshotagainttask);
        }

        [HttpGet]
        public ActionResult CompanySetting()
        {
            SetLogo();
            GetOffices();
            return View();
        }

        [HttpPost]
        public ActionResult UpdateCompany(tblcompany company)
        {
            GetOffices();
            string Name = Path.GetFileName(company.Logo.FileName);
            string path = Server.MapPath("~/AdminAssets/Images");
            string fullpath = Path.Combine(path, Name);
            company.Logo.SaveAs(fullpath);
            int companyId = (Convert.ToInt32(Session["company-id"]));
            tbl_company obj = new tbl_company();
            obj.Name = company.Name;
            obj.Logo = company.Logo.FileName;
            dbcontext.sp_updatecompanysetting(obj.Name, obj.Logo, companyId);
            dbcontext.SaveChanges();
            return RedirectToAction("index");
        }

        public sp_getcompanydetail_Result1 Getcompanydetail(int? companyId)
        {
            var list = dbcontext.sp_getcompanydetail(companyId).FirstOrDefault();
            return list;
        }


        [HttpGet]
        public ActionResult UpdateProfile()
        {
            SetLogo();
            GetOffices();
            int userid = Convert.ToInt32(Session["additionalId"]);
            var list = dbcontext.USERS.Where(x => x.Id == userid).SingleOrDefault();
            return View(list);
        }
        [HttpPost]
        [ActionName("UpdateProfile")]
        public ActionResult UpdateProfile_post(USER user)
        {
            USER obj = new USER();
            obj.Username = user.Username;
            obj.Password = user.Password;
            obj.Email = user.Email;
            obj.Mobile = user.Mobile;
            obj.Id = Convert.ToInt32(Session["additionalId"]);
            dbcontext.sp_updateprofile(obj.Username, obj.Password, obj.Email, obj.Mobile, obj.Id);
            dbcontext.SaveChanges();
            return RedirectToAction("UpdateProfile");
        }

        [HttpGet]
        public ActionResult Download()
        {
            SetLogo();
            GetOffices();
            var list = dbcontext.tbl_download.ToList();
            return View(list);
        }
        [HttpGet]
        public ActionResult Downloadfile(string file_name)
        {
            var fileName = string.Format("{0}Archieve.zip", DateTime.Today.Date.ToString("dd-MM-yyyy") + "_1");
            var tempOutPutPath = Server.MapPath(Url.Content("~/Download/")) + fileName;
            using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
            {
                s.SetLevel(9); // 0-9, 9 being the highest compression  
                byte[] buffer = new byte[4096];
                var ImageList = new List<string>();
                ImageList.Add(Server.MapPath("/Download/DesktopApp/" + file_name));
                // ImageList.Add(Server.MapPath("/Images/02.jpg"));
                for (int i = 0; i < ImageList.Count; i++)
                {

                    ZipEntry entry = new ZipEntry(Path.GetFileName(ImageList[i]));
                    entry.DateTime = DateTime.Now;
                    entry.IsUnicodeText = true;
                    s.PutNextEntry(entry);

                    using (FileStream fs = System.IO.File.OpenRead(ImageList[i]))
                    {
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0, buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }
                s.Finish();
                s.Flush();
                s.Close();
            }
            byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
            if (System.IO.File.Exists(tempOutPutPath))
                System.IO.File.Delete(tempOutPutPath);
            if (finalResult == null || !finalResult.Any())
                throw new Exception(String.Format("No Files found with Image"));
            return File(finalResult, "application/zip", fileName);
        }
        public ActionResult Downloadexefromuser()
        {
            return View();
        }

        //byte[] GetFile(string s)
        //{
        //    byte[] fs = System.IO.File.ReadAllBytes(s);
        //    byte[] data = new byte[fs.Length];
        //    int br = fs.Read(data, 0, data.Length);
        //    if (br != fs.Length)
        //        throw new System.IO.IOException(s);
        //    return data;
        //}

        public JsonResult GetTimeLinedatas(int? id)
        {
            var todowork = dbcontext.Sp_GetTodayTimelineData(id).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();
                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;


                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());

                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   



                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });

            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTimeLinedatalast7Days(int? id)
        {
            var todowork = dbcontext.Sp_Getlast7DayTimelineData(id).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();
                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;

                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());

                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   
                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });
            }

            return Json(arrlist, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetTimeLinedataYesterday(int? id)
        {
            var todowork = dbcontext.Sp_GetYesterDayTimelineData(id).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();
                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;
                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());

                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("yyyy-dd-MM HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   
                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });
            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTimeLinedatalast30Days(int? id)
        {
            var todowork = dbcontext.Sp_Getlast30DayTimelineData(id).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();


                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;


                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());

                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   
                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });
            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetcompanytodayTimeLinedatas()
        {
            var Nowdate = DateTime.Now.Date;
            var todaydate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            int companyid = Convert.ToInt32(Session["company-id"]);
            var todowork = dbcontext.Sp_GetTodayoverallTimelineData(todaydate, companyid).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();


                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;


                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());

                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   



                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });

            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetcompanyyesterdayTimeLinedatas()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var Nowdate = DateTime.Now.Date;
            var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            var dayofnowdate = Nowdate.Date.Day;
            var dayofpreviousdate = 0;
            var month = 0;
            var day = "";
            if (dayofnowdate == 1)
            {
                dayofpreviousdate = 30 - 1;
                month = Nowdate.Date.Month;
                month = month - 1;
            }
            else
            {
                day = Convert.ToString(dayofnowdate - 1);
                month = Nowdate.Date.Month;
            }
            var year = Nowdate.Date.Year;
            if (dayofnowdate <= 9)
            {
                day = "0" + Convert.ToString(dayofpreviousdate);
            }
            var enddate = year + "-" + month + "-" + day;
            var todowork = dbcontext.Sp_GetyesterdayoverallTimelineData(enddate, companyid).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();


                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;


                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());

                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   



                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });

            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Getcompanypast7daysLinedatas()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var Nowdate = DateTime.Now.Date;
            var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            var dayofnowdate = Nowdate.Date.Day;
            var dayofpreviousdate = 0;
            var month = 0;
            var day = "";
            if (dayofnowdate == 1)
            {
                dayofpreviousdate = 30 - 7;
                month = Nowdate.Date.Month;
                month = month - 1;
            }
            else
            {
                dayofpreviousdate = dayofnowdate - 7;
                month = Nowdate.Date.Month;
            }
            var year = Nowdate.Date.Year;
            if (dayofpreviousdate <= 9)
            {
                day = "0" + Convert.ToString(dayofpreviousdate);
            }
            var enddate = year + "-" + month + "-" + day;
            var todowork = dbcontext.Sp_Getpast7daysoverallTimelineData(startdate, enddate, companyid).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();
                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;
                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());
                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   



                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });

            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Getcompanypast30daysLinedatas()
        {
            int companyid = Convert.ToInt32(Session["company-id"]);
            var Nowdate = DateTime.Now.Date;
            var startdate = String.Format("{0:yyyy-MM-dd}", Nowdate);
            var dayofnowdate = Nowdate.Date.Day;
            var monthofnowdate = Nowdate.Date.Month;
            var monthofpreviousdate = monthofnowdate - 1;
            var year = Nowdate.Date.Year;
            var enddate = year + "-" + monthofpreviousdate + "-" + dayofnowdate;
            var todowork = dbcontext.Sp_Getpast30daysoverallTimelineData(startdate, enddate, companyid).ToList();
            for (int i = 0; i < todowork.Count(); i++)
            {
                // Getting Date from loop//
                string date = todowork[i].date.ToString();
                // Getting start Time of Task //
                DateTime Starttime = DateTime.Parse(todowork[i].TodayDate.ToString());
                string Stime = Starttime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                string DateofReport = Starttime.ToString("dd-MM-yyyy", CultureInfo.CurrentCulture);
                ///double stofTask = TimeSpan.Parse(Starttime.ToString()).Seconds;
                // Getting Session time of Task //
                TimeSpan sessiontime = TimeSpan.Parse(todowork[i].Total_time.ToString());
                //Getting End Time of Task //
                DateTime endtime = Starttime.Add(sessiontime);
                string ETime = endtime.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                //  double EndofTask = TimeSpan.Parse(endtime.ToString()).Hours;

                //Getting Idle Time //
                ///   string idletime = todowork[i].Idle_time.ToString();
                ///   



                arrlist.Add(new proclass { name = DateofReport, fromDate = Stime, toDate = ETime });

            }
            return Json(arrlist, JsonRequestBehavior.AllowGet);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ezilineezitaskernew.Models
{
    public class proclass
    {
        public string totaluserhours { get; set; }
        public List<Nullable<int>> totaluserproject { get; set; }
        public List<Nullable<int>> totalusertask { get; set; }
        public List<string> totaluserbiohours { get; set; }
        public int workid { get; set; }
        public string UserName { get; set; }
        public string start_time { get; set; }
        public string Date { get; set; }
        public string End_time { get; set; }
        public string Total_time { get; set; }
        public string Today_date { get; set; }
        public string Task_id { get; set; }
        public string Task { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Project_name { get; set; }
        public string Net_total_time { get; set; }
        public string Project_id { get; set; }
        public string User_id { get; set; }
        public int CompanyId { get; set; }

        public string todayhours { get; set; }
        public string todayhoursofuser { get; set; }
        public string idlehours { get; set; }
        public List<Nullable<int>> todayproject { get; set; }
        public List<Nullable<int>> todayprojectofuser { get; set; }
        public List<Nullable<int>> todaytasks { get; set; }
        public List<Nullable<int>> todaytasksofuser { get; set; }
        public List<string> Todaybiomatric { get; set; }
        public List<string> Todaybiomatricofuser { get; set; }

        public string _7dayshours { get; set; }
        public string yesterdayhour { get; set; }
        public string _30dayshours { get; set; }
        public string _7dayshoursofuser { get; set; }
        public Nullable<int> _7dayproject { get; set; }
        public Nullable<int> _30dayprojects { get; set; }
        public Nullable<int> yesterdayproject { get; set; }
        public Nullable<int> _7dayprojectofuser { get; set; }
        public Nullable<int> _7daytasks { get; set; }
        public Nullable<int> _30daytasks { get; set; }
        public Nullable<int> yesterdaytask { get; set; }
        public Nullable<int> _7daytasksofuser { get; set; }
        public string _7daybiohours { get; set; }
        public string _30daybiohours { get; set; }
        public string yesterdaybiohours { get; set; }
        public string _7daybiohoursofuser { get; set; }

        public List<string> daterangehours { get; set; }
        public List<Nullable<int>> daterangeprojects { get; set; }
        public List<Nullable<int>> daterangetasks { get; set; }
        public List<string> daterangebiohours { get; set; }


        public List<string> daterangehoursbyusername { get; set; }
        public List<Nullable<int>> daterangeprojectsbyusername { get; set; }
        public List<Nullable<int>> daterangetasksbyusername { get; set; }
        public List<string> daterangebiohoursbyusername { get; set; }



        //chart properties
        public string name { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}
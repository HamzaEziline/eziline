//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ezilineezitaskernew.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class USER
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string emp_finger { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public Nullable<int> OfficeID { get; set; }
        public string Cnic { get; set; }
        public string Address { get; set; }
        public string UserImage { get; set; }
    }
}

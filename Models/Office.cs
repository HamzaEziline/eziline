﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ezilineezitaskernew.Models
{
    [MetadataType(typeof(Office))]
    public partial class OfficeLoc
    {
        public List<Nullable<int>> Usercount { get; set; }
        public string Hours { get; set; }
        public string OfficeImage_str { get; set; }
    }
    public class Office
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter notes")]
        public string Notes { get; set; }
        [Required(ErrorMessage = "Please select status")]
        public string Status { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public Nullable<int> Additionalid { get; set; }

        public HttpPostedFileBase OfficeImage { get; set; }

        
        public string OfficeImage_str { get; set; }
        public List<Nullable<int>> Usercount { get; set; }
        public string Hours { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ezilineezitaskernew.Models
{
    [MetadataType(typeof(Usermetadata))]
    public partial class USER
    {
        public int RoleID { get; set; }
    }
    public class Usermetadata
    {
        public int Id { get; set; }
        public string Address { get; set; }

        public string Mobile { get; set; }
        public int RoleID { get; set; }
 
        public string Password { get; set; }

        public string Email { get; set; }
        public string Cnic { get; set; }

        
        public String Name { get; set; }

       
        public Nullable<int> CompanyID { get; set; }
   
        public string Gender { get; set; }
        public string Description { get; set; }
      
        public byte [] Image { get; set; }
   
        public string UserImage { get; set; }

        public Nullable<int> OfficeID { get; set; }

        public Nullable<bool> Status { get; set; }
        public string Username { get; set; }

    }

}
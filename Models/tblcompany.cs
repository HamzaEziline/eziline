﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ezilineezitaskernew.Models
{
    [MetadataType(typeof(Office))]
    public partial class tbl_company
    {

    }
    public class tblcompany
    {
        public int CompanyID { get; set; }
        public string Name { get; set; }
        public HttpPostedFileBase Logo { get; set; }
        public string Address { get; set; }
        public string AdminID { get; set; }
        public string Description { get; set; }
    }
}